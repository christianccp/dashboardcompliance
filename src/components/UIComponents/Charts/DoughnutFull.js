import { Doughnut } from 'vue-chartjs'
import pluginChartText from './plugins/plugin-chart-text';


const defaultOptions = {
  title:{
    fontStyle:"normal",
    sidePadding:30
  },
  cutoutPercentage: 60,//grosor de circunferencia
  legend: {
    display: false
  },
  backgroundColor:'green',
  tooltips: {
    enabled: false
  },
  animation: {
    animateRotate: true,
    animateScale: true
  },

  scales: {
    yAxes: [{

      ticks: {
        display: false
      },
      gridLines: {
        drawBorder: true,
        zeroLineColor: "transparent",
        color: 'rgba(255,255,255,0.05)'
      }

    }],

    xAxes: [{
      barPercentage: 1.6,
      gridLines: {
        drawBorder: false,
        color: 'rgba(255,255,255,0.1)',
        zeroLineColor: "transparent"
      },
      ticks: {
        display: false,
      }
    }]
  },
};

export default {
  name: 'doughnut',
  extends: Doughnut,
  props: {
    labels: {
      type: [Object, Array],
      description: 'Chart labels. This is overridden when `data` is provided'
    },
    datasets: {
      type: [Object, Array],
      description: 'Chart datasets. This is overridden when `data` is provided'
    },
    data: {
      type: [Object, Array],
      description: 'Chart.js chart data (overrides all default data)'
    },
    color: {
      type: [String, Array],
      description: 'Chart color. This is overridden when `data` is provided'
    },
    extraOptions: {
      type: Object,
      description: 'Chart.js options'
    },
    title: {
      type: [String, Array],
      description: 'Chart title'
    },
  },
  watch: {
    datasets(newValue, oldValue) {
      this.addPlugin(pluginChartText);
      let chartOptions = Object.assign(defaultOptions, this.extraOptions || {})
      chartOptions.elements.center.text = this.data ? `${this.data[0]}% ` : null;
      chartOptions.onClick = this.onClickChart
      this.renderChart({
        labels: this.labels || [],
        datasets: newValue ? newValue : [{
          pointRadius: 0,
          pointHoverRadius: 0,
          backgroundColor: this.color || ['#e67', '#5e6', '#ef4'],
          borderWidth: 0,
          data: this.data || []
        }]
      }, chartOptions);
    },
    data(newValue, oldValue) {
      this.addPlugin(pluginChartText);
      let chartOptions = Object.assign(defaultOptions, this.extraOptions || {})
      chartOptions.onClick = this.onClickChart
      this.renderChart({
        labels: this.labels || [],
        datasets: this.datasets ? this.datasets : [{
          pointRadius: 0,
          pointHoverRadius: 0,
          backgroundColor: this.color || ['#e67', '#5e6', '#ef4'],
          borderWidth: 0,
          data: newValue || []
        }]
      }, chartOptions);
    }
  },
  mounted() {
    this.addPlugin(pluginChartText);
    let chartOptions = Object.assign(defaultOptions, this.extraOptions || {})
    chartOptions.onClick = this.onClickChart
    this.renderChart({
      labels: this.title || [],
      datasets: this.datasets ? this.datasets : [{
        pointRadius: 0,
        pointHoverRadius: 0,
        backgroundColor: this.color || ['#e67', '#5e6', '#ef4'],
        borderWidth: 0,
        data: this.data || []
      }]
    }, chartOptions);
  },
  methods: {
    onClickChart(point, event) {
      this.$emit('onClickChart', event[0]._index)
    }
  }
}
