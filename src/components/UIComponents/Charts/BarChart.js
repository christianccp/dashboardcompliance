import {Bar} from 'vue-chartjs'
import {hexToRGB} from "./utils";
import * as functions from 'src/plugins/generalFunctions.js'
import ChartDataLabels from 'chartjs-plugin-datalabels';

let defaultOptions = {

  tooltips: {
    tooltipFillColor: "rgba(0,0,0,0.5)",
    tooltipFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
    tooltipFontSize: 14,
    tooltipFontStyle: "normal",
    tooltipFontColor: "#fff",
    tooltipTitleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
    tooltipTitleFontSize: 14,
    tooltipTitleFontStyle: "bold",
    tooltipTitleFontColor: "#fff",
    tooltipYPadding: 6,
    tooltipXPadding: 6,
    tooltipCaretSize: 8,
    tooltipCornerRadius: 6,
    tooltipXOffset: 10,
  },


  legend: {

    display: false
  },
  scales: {

    yAxes: [{
      ticks: {
        fontColor: "#9f9f9f",
        fontStyle: "bold",
        beginAtZero: true,
        maxTicksLimit: 5,
        padding: 20
      },
      gridLines: {
        zeroLineColor: "transparent",
        display: true,
        drawBorder: false,
        color: '#9f9f9f',
      }

    }],
    xAxes: [{
      barPercentage: 0.4,
      categoryPercentage: 2,
      gridLines: {
        zeroLineColor: "transparent",
        display: false,
        drawBorder: false,
        color: 'transparent',
      },
      ticks: {
        padding: 20,
        fontColor: "#9f9f9f",
        fontStyle: "bold"
      }
    }]
  }
};
export default {
  name: 'bar-chart',
  extends: Bar,
  props: {
    useInitials:{
      type:Boolean,
      default: false,
      description: 'Set tru if you want see initial in info of axes, false if you want see original info'
    },
    labels: {
      type: [Object, Array],
      description: 'Chart labels. This is overridden when `data` is provided'
    },
    datasets: {
      type: [Object, Array],
      description: 'Chart datasets. This is overridden when `data` is provided'
    },
    data: {
      type: [Object, Array],
      description: 'Chart.js chart data (overrides all default data)'
    },
    color: {
      type: String,
      description: 'Chart color. This is overridden when `data` is provided'
    },
    bgColor:{
      type:[Object, Array],
      description: ' Bar Chart color. This is overridden when `data` is provided'
    },
    borderColor:{
      type: String,
      description: 'Bar border color. This is overridden when `data` is provided'
    },
    fColor:{
      type: String,
      description: 'Font color. This is overridden when `data` is provided'
    },
    barPercen:{
      type: Number,
      default: 0,
      description: 'Chart height'
    },
    labelRotation:{
      type: Number,
      default: 0,
      description: 'Chart height'
    },
    extraOptions: {
      type: Object,
      description: 'Chart.js options'
    },
    title: {
      type: String,
      description: 'Chart title'
    },
  },
  watch:{
    datasets(newValue, oldValue){    
      let fallBackColor = '#f96332';
      let color = this.color || fallBackColor;
      const ctx = document.getElementById(this.chartId).getContext('2d');
      const gradientFill = ctx.createLinearGradient(0, 170, 0, 50);
      gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
      let chartOptions = Object.assign(defaultOptions, this.extraOptions || {})
      var callbackTitle = {
        enabled:true,
        callbacks:{
          title: ((tooltipItems, data) =>{
            return data.labels[tooltipItems[0].index]
          })
        }
      }
      chartOptions.tooltips= this.useInitials ? callbackTitle : {}

      var callbackLabel = {
        callback: (value, data, values) =>{
          return functions.getAbbrev(value)
        }
      }
      chartOptions.scales.xAxes[0].ticks= this.useInitials ? callbackLabel : {}
      this.renderChart({
        labels: this.labels || [],
        datasets: newValue ? newValue : [{
          label: this.title || '',
          backgroundColor: this.bgColor || gradientFill,
          borderColor: this.borderColor || color,
          pointBorderColor: "#FFF",
          pointBackgroundColor: color,
          pointBorderWidth: 2,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 1,
          pointRadius: 4,
          fill: true,
          borderWidth: 1,
          data: this.data || []
        }]
      }, chartOptions);
    },
    data(newValue, oldValue){    
      let fallBackColor = '#f96332';
      let color = this.color || fallBackColor;
      const ctx = document.getElementById(this.chartId).getContext('2d');
      const gradientFill = ctx.createLinearGradient(0, 170, 0, 50);
      gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
      let chartOptions = Object.assign(defaultOptions, this.extraOptions || {})
      var callbackTitle = {
        enabled:true,
        callbacks:{
          title: ((tooltipItems, data) =>{
            return data.labels[tooltipItems[0].index]
          })
        }
      }
      chartOptions.tooltips= this.useInitials ? callbackTitle : {}

      var callbackLabel = {
        callback: (value, data, values) =>{
          return functions.getAbbrev(value)
        }
      }
      chartOptions.scales.xAxes[0].ticks= this.useInitials ? callbackLabel : {}
      this.renderChart({
        labels: this.labels || [],
        datasets: this.datasets ? this.datasets : [{
          label: this.title || '',
          backgroundColor: this.bgColor || gradientFill,
          borderColor: this.borderColor || color,
          pointBorderColor: "#FFF",
          pointBackgroundColor: color,
          pointBorderWidth: 2,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 1,
          pointRadius: 4,
          fill: true,
          borderWidth: 1,
          data: newValue || []
        }]
      }, chartOptions);
    }
  },
  mounted() {
    let fallBackColor = '#f96332';
    let color = this.color || fallBackColor;
    const ctx = document.getElementById(this.chartId).getContext('2d');
    const gradientFill = ctx.createLinearGradient(0, 170, 0, 50);
    gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
    let chartOptions = Object.assign(defaultOptions, this.extraOptions || {})
    this.addPlugin(ChartDataLabels);
    var callbackTitle = {
      enabled:true,
      callbacks:{
        title: ((tooltipItems, data) =>{
          return data.labels[tooltipItems[0].index]
        })
      }
    }
    chartOptions.tooltips= this.useInitials ? callbackTitle : {}

    var callbackLabel = {
      callback: (value, data, values) =>{
        return functions.getAbbrev(value)
      }
    }
    chartOptions.scales.xAxes[0].ticks= this.useInitials ? callbackLabel : {}
    this.renderChart({
      labels: this.labels || [],
      datasets: this.datasets ? this.datasets : [{
        label: this.title || '',
        backgroundColor: this.bgColor || gradientFill,
        borderColor: this.borderColor || color,
        pointBorderColor: "#FFF",
        pointBackgroundColor: color,
        pointBorderWidth: 2,
        pointHoverRadius: 4,
        pointHoverBorderWidth: 1,
        pointRadius: 4,
        fill: true,
        borderWidth: 1,
        data: this.data || []
      }]
    }, chartOptions);
  }
}
