import SidebarFaq from './SideBarFaq.vue'
import SidebarItemFaq from './SidebarItemFaq.vue'

const SidebarFaqStore = {
  showSidebar: false,
  sidebarLinks: [],
  isMinimized: false,
  displaySidebar (value) {
    this.showSidebar = value
  },
  toggleMinimize () {
    document.body.classList.toggle('faq-mini')
    // we simulate the window Resize so the charts will get updated in realtime.
    const simulateWindowResize = setInterval(() => {
      window.dispatchEvent(new Event('resize'))
    }, 180)

    // we stop the simulation of Window Resize after the animations are completed
    setTimeout(() => {
      clearInterval(simulateWindowResize)
    }, 1000)

    this.isMinimized = !this.isMinimized
  }
}

const FaqPlugin = {

  install (Vue, options) {
    if (options && options.sidebarLinks) {
      SidebarFaqStore.sidebarLinks = options.sidebarLinks
    }
    Vue.mixin({
      data () {
        return {
          sidebarFaqStore: SidebarFaqStore
        }
      }
    })

    Vue.prototype.$faq = SidebarFaqStore
    Object.defineProperty(Vue.prototype, '$faq', {
      get () {
        return this.$root.sidebarFaqStore
      }
    })
    Vue.component('faq-bar', SidebarFaq)
    Vue.component('faq-item', SidebarItemFaq)
  }
}

export default FaqPlugin