import api from '../api.js'

let path = '/subrisk'

export default {
  getSubrisks (paramsApi) {
    let { access_token, moduleId } = paramsApi
    return new Promise((resolve, reject) => {
      api.get(path,{ params: {access_token, moduleId }})
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  updateSubrisk (paramsApi) {
    let { access_token,subrisk} = paramsApi
    return new Promise((resolve, reject) => {
      const obj= { access_token, ...subrisk}
      api.put(path  + '/' + subrisk._id, JSON.stringify(obj) )
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      }) 
    })
  },
  deleteSubrisk (paramsApi) {
    let { access_token, id} = paramsApi
    return new Promise((resolve, reject) => {
      api.delete(path + '/' + id, { params: { access_token }} )
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      }) 
    })
  },
  addSubrisk (paramsApi) {
    let { access_token,subrisk, moduleId} = paramsApi
    return new Promise((resolve, reject) => {
      const obj= { access_token, ...subrisk, moduleId}
      api.post(path, JSON.stringify(obj) )
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      }) 
    })
  },
  setResidualLevels (paramsApi) {
    let { access_token, subrisks} = paramsApi
    return new Promise((resolve, reject) => {
      api.put(path + "/set/residualLevels", {access_token, subrisks} )
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      }) 
    })
  },
  setProceder (paramsApi) {
    let { access_token, subrisks} = paramsApi
    return new Promise((resolve, reject) => {
      api.put(path + "/set/proceder", {access_token, subrisks} )
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      }) 
    })
  }
}
