import api from '../api.js'

const path = '/subrisk'

export default {
    /*setSubriskParams(access_token,subrisk_id, datos) {
        const objeto = { access_token: access_token, 
                        levelInhAvg:{value:datos.levelAvg}, 
                        probabilityAvg:{value: datos.probAvg}, 
                        impactAvg:{value:datos.impactAvg},
                        params: datos.params }
        return new Promise((resolve, reject) => {
          api.put(path + '/updateparams/' + subrisk_id, objeto )
          .then(response => { resolve(response) })
          .catch(error => {
            console.log(error)
            reject(error)
         })
        })
    },*/
    getSubriskParams (access_token, subrisk_id) {
      return new Promise((resolve, reject) => {
        api.get(path+'/params/' + subrisk_id,{ params: { access_token }})
        .then(response => { resolve(response) })
        .catch(error => {
          console.log(error)
          reject(error)
        })
      })
    },
    addSubriskParams(paramsApi) {
      let { subriskId } = paramsApi
      return new Promise((resolve, reject) => {
        api.put(path + '/' + subriskId + '/addParams', paramsApi )
        .then(response => { 
          resolve(response) 
        })
        .catch(error => {
          console.log(error)
          reject(error)
        })
      })
    },
    deleteSubriskParams(paramsApi) {
      let { subriskId } = paramsApi
      return new Promise((resolve, reject) => {
        api.put(path + '/' + subriskId + '/deleteParams', paramsApi )
        .then(response => { resolve(response) })
        .catch(error => {
          console.log(error)
          reject(error)
        })
      })
    },  
    updateSubriskParams(paramsApi) {
      let { subriskId } = paramsApi
      return new Promise((resolve, reject) => {
        api.put(path + '/' + subriskId + '/updateParameters', paramsApi )
        .then(response => { resolve(response) })
        .catch(error => {
          console.log(error)
          reject(error)
        })
      })
    },
}
