import api from '../api.js'

let path = '/systemlog'

export default {
  getLog (paramsApi) {
    let { access_token } = paramsApi
    return new Promise((resolve, reject) => {
      api.get(path,{ params: {access_token }})
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  addLog (paramsApi) {
    let { ...systemLog } = paramsApi
    return new Promise((resolve, reject) => {
      const obj= { ...systemLog }
      api.post(path, JSON.stringify(obj) )
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      }) 
    })
  },
}
