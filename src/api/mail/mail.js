import api,{ENDPOINT_BACK} from '../api.js'
import {URL_FRONT_DASH} from '../../varibleProd'
const path = '/mail'
export default {
    sendMail(paramsApi){
      return new Promise((resolve, reject) => {
        let { correo, link, recovery} = paramsApi
        let liga = URL_FRONT_DASH + '/password?' + link
        api.post(path + '/sendLink',{ correo: correo, link: liga, recovery: recovery})
        .then(response => { resolve(response) })
        .catch(error => {
          reject(error)
        })
      })
    },
    blockUser(paramsApi){
      return new Promise((resolve, reject) => {
        let { correo, link} = paramsApi
        let liga = URL_FRONT_DASH + '/recoverymail'
        api.post(path + '/setBlockUser',{ correo: correo, link: liga})
        .then(response => { resolve(response) })
        .catch(error => {
          reject(error)
        })
      })
    },
}

