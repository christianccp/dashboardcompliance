import api from '../api.js'
const path = '/users'
const master_token = {access_token: "9laxjjT3iyTJ5B3UYJaFftUmEcmVuWvv"}

export default {    
  getOrgMembers(access_token){
    return new Promise((resolve, reject) => {
      api.get(path+'/getorgmembers',{params:{access_token}})
     .then(response => { 
          resolve(response);
      })
    .catch(error => {
        console.log(error)
      reject(error)
      })
    })
  },
  getUsuario() {
    const datapi=this;
    const token_id = $cookies.get("token")
    return new Promise((resolve, reject) => {
    api.get(path+'/me?access_token='+token_id)
   .then(response => { 
        resolve(response);
    })
  .catch(error => {
      console.log(error)
    reject(error)
    })
  })
  },
  updateUsuario (token_id,user) { 
    return new Promise((resolve, reject) => {
      api.put(path+'/'+token_id, user)
      .then(response => { resolve(response);
       })
      .catch(error => {
        console.log("Catch:"+error)
        reject(error)
      }) 
    });
  },
  insertPassword (user, password) { 
    return new Promise((resolve, reject) => {
      const obj = { password }
      api.put(path + '/' + user + '/setPassword', JSON.stringify(obj))
      .then(response => { resolve(response);
       })
      .catch(error => {
        console.log("Catch:"+error)
        reject(error)
      }) 
    });
  },
  assignRole(params) { 
    return new Promise((resolve, reject) => {
      var {access_token, role, id} = params
      api.put(path + '/assignRole/' + id, {access_token, role})
      .then(response => { resolve(response);
       })
      .catch(error => {
        console.log("Catch:"+error)
        reject(error)
      }) 
    });
  },
  changeProfilePhoto(params) { 
    return new Promise((resolve, reject) => {
      var {_id, file, access_token} = params
      var formData = new FormData()
      var config = {
        headers: {
          'Content-Type': file.type
        },
        timeout: 50000
      }
      formData.append('Content-type', file.type)
      formData.append('file', file)
      formData.append('_id', _id)
      api.post(path + '/profile/image',formData, config)
      .then(response => { resolve(response);
       })
      .catch(error => {
        console.log("Catch:"+error)
        reject(error)
      }) 
    });
  },
  getDataUsuario(id) {
    return new Promise((resolve, reject) => {
      api.get(path + '/' + id)
      .then(resp => { 
        resolve(resp);
      })
      .catch(error => {
       console.log(error)
       reject(error)
      })
    })
  },
  deleteUser(params) {
    return new Promise((resolve, reject) => {      
      var {access_token, id} = params
      api.delete(path + '/' + id, {params: {access_token: access_token}})
      .then(resp => { 
        resolve(resp);
      })
      .catch(error => {
       console.log(error)
       reject(error)
      })
    })
  },
  setFlagsUser(params){
    return new Promise((resolve, reject) => {      
      var {access_token, flags} = params
      api.put(path + '/set/Flag', {access_token, flags})
      .then(resp => { 
        resolve(resp);
      })
      .catch(error => {
       console.log(error)
       reject(error)
      })
    })
  },
  getUserExist(params){
    return new Promise((resolve, reject) => {      
      api.get(path+'/user/exist', {params: {email:params.email}})
      .then(resp => { 
        resolve(resp);
      })
      .catch(error => {
       
       reject(error)
      })
    })
  },
  clearPassword(params){
    return new Promise((resolve, reject) => {      
      api.put(path+'/set/emptypass', {email:params.email})
      .then(resp => { 
        resolve(resp);
      })
      .catch(error => {
       reject(error)
      })
    })
  },
  setUser (addUser) {
    return new Promise((resolve, reject) => {
      const obj= { master_token, addUser}
      api.post(path, Object.assign(master_token, addUser))
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  getExpirationDateLink(paramsAPI){
    return new Promise((resolve, reject) => {            
      api.get(path+'/expiration/DateLink', {params: {id:paramsAPI}})
      .then(resp => { 
        resolve(resp);
      })
      .catch(error => {
       
       reject(error)
      })
    })
  }
}
