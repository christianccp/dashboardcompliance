import api from '../api.js'

let path = '/folders'

export default {
  getFolders (access_token) {
    return new Promise((resolve, reject) => {
      api.get(path,{ params:{access_token}})
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  insertFolder (paramsApi) {
    return new Promise((resolve, reject) => {
      api.post(path, paramsApi )
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      }) 
    })
  },
  updateFolder (paramsApi) {
    return new Promise((resolve, reject) => {
      let { access_token, id, name } = paramsApi
      api.put(path + '/' + id, { access_token, name })
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  deleteFolder (paramsApi) {
    let { access_token, id} = paramsApi
    return new Promise((resolve, reject) => {
      api.delete(path+'/'+id, {params:{access_token}} )
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      }) 
    })
  },
}