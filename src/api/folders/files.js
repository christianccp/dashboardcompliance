import api from '../api.js'

let path = '/files'

export default {
  getFiles (paramsApi) {
    let { access_token, folderId } = paramsApi
    return new Promise((resolve, reject) => {
      api.get(path + '/by/folder/', { params:{access_token, folderId}})
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  insertFiles (paramsApi) {
    return new Promise((resolve, reject) => {
      let { access_token, data } = paramsApi
      let file = data.file
      var formData = new FormData()
      var config = {
        headers: {
          'Content-Type': file.type
        },
        timeout: 150000
      }
      formData.append('Content-type', file.type)
      formData.append('file', file)
      formData.append('origin', data.origin)
      formData.append('moduleId', data.moduleId)
      if( data.task_id !== undefined){
        formData.append('task_id', data.task_id)
      }      
      formData.append('folderId', data.folderId)
      formData.append('areaInvolved', JSON.stringify(data.areaInvolved))

      api.post(path + '/upload/file?access_token=' + access_token, formData, config )
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      }) 
    })
  },
  putVersionFile (paramsApi) {
    return new Promise((resolve, reject) => {
      let { access_token, data } = paramsApi
      let file = data.file
      var formData = new FormData()
      var config = {
        headers: {
          'Content-Type': file.type
        },
        timeout: 50000
      }
      formData.append('Content-type', file.type)
      formData.append('file', file)
      formData.append('key_file', data.key_file)

      api.put(path + '/update/file?access_token=' + access_token, formData, config )
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      }) 
    })
  },
  updateFiles (paramsApi) {
    return new Promise((resolve, reject) => {
      api.put(path + '/' + paramsApi.id, paramsApi)
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  setLogFiles (paramsApi) {
    return new Promise((resolve, reject) => {
      api.put(path + '/' + paramsApi.id+'/updateLog', paramsApi)
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  deleteFile (paramsApi) {
    let { access_token, id} = paramsApi
    return new Promise((resolve, reject) => {
      api.delete(path+'/'+id, {params:{access_token}} )
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      }) 
    })
  },
  deleteVersionFile (paramsApi) {
    const {access_token, keyFile, versionId} = paramsApi
    return new Promise((resolve, reject) => {
      api.delete(path+'/delete/version/file', {params: paramsApi, data: {keyFile, versionId}})
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      }) 
    })
  }, 
}