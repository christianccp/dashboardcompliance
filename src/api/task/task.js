import api from '../api.js'
import store from '@/store';

const FileSaver = require('file-saver');
const path = '/tasks'


export default {
    // si se ocupa
    postTask(paramsApi){
      return new Promise((resolve, reject) => {
        let {access_token, tasks, moduleId} = paramsApi
        api.post(path,{ access_token, moduleId, tasks})
        .then(response => { resolve(response) })
        .catch(error => {
          reject(error)
        })
      })
    },
    // si se ocupa
    getTasks(paramsApi){
      return new Promise((resolve, reject) => {
        let {access_token, moduleId} = paramsApi
        api.get(path + "/get/tasks", { params: { access_token, moduleId }})
        .then(response => { resolve(response) })
        .catch(error => {
          reject(error)
        })
      })
    },
    putTasksAuditoria(paramsApi){
      let {id, auditoria, moduleId } = paramsApi
     // console.log(JSON.stringify(auditoria,null,2)) 
      return new Promise((resolve, reject) => {
        api.put(path+"/"+id+"/createAudit", auditoria)
        .then(response => { 
          resolve(response);
        })
       .catch(error => {
       //  console.log("Catch:"+error)
         reject(error)
       }) 
     });
    },

    // si se ocupa
    putFiles(paramsApi){
      return new Promise((resolve, reject) => {
        var files=paramsApi.files
        var access_token=paramsApi.access_token
        var formData = new FormData()
        var config={
          headers: {
            'Content-Type': files.type,
          },
          onUploadProgress: (progressEvent) => {
            if (progressEvent.lengthComputable) { 
              const totalLength = progressEvent.total
              let pct=Math.round((progressEvent.loaded / totalLength)*100 )-4;
              console.log("Carga:"+pct+"%")
              store.commit('task/ADD_LOADING', pct)
            }
          }, 
          timeout: 150000
        }
        formData.append('Content-type', file.type)
        formData.append('file', files)
        formData.append('origin',paramsApi.origin)
        formData.append('moduleId', 'anticorruption')
        formData.append('task_id',paramsApi.task_id)
        api.post("/files/upload/file?access_token="+access_token, formData,config)
        .then(response => { 
          resolve(response);
        })
       .catch(error => {
         console.log("Catch:"+error)  
         reject(error)
       }) 
     });
    },
    deleteFiles(paramsApi){
      let { access_token, moduleId, taskId, keyFile} = paramsApi
           
      return new Promise((resolve, reject) => {
        api.delete('/files/delete/files', {params:{ access_token,moduleId, taskId, keyFile}, data:{ access_token,moduleId, taskId, keyFile}})
        .then(response => {
          resolve(response) })
        .catch(error => {
          console.log("catch error"+error)
          reject(error)
        }) 
      })

    },
    downloadFiles(paramsApi) {
      let { access_token, key} = paramsApi;
      return new Promise((resolve,reject) => {
        api.get('files/'+key+ '/get/File', {
          responseType: 'arraybuffer',
          params:{access_token},
          timeout:150000
        }) 
        .then((response) => {
          const blob = new Blob([response.data], {
            type: response.headers['content-type'],
          });
          FileSaver.saveAs(blob, paramsApi.name);
          resolve(response)
        })
        .catch(error => {
          console.log("catch error"+error)
          reject(error)
        }) 
      })
    },
    addColumnAux(paramsApi){
      let {fieldName}=paramsApi
    //  console.log("Inside addcolumnAux Api"+JSON.stringify(paramsApi))
    //  console.log(path)
      return new Promise((resolve, reject) => {
        api.put(path+"/new/field", paramsApi)
        .then(response => { 
    //      console.log("Se ha creado"+fieldName)
          resolve(response);
        })
       .catch(error => {
      //   console.log("Catch:"+error)
         reject(error)
       }) 
     });
    },
    removeColumnAux(paramsApi){
      let {fieldName}=paramsApi
  //    console.log("Inside removecolumnAux Api"+JSON.stringify(paramsApi))
  //    console.log(path)
      return new Promise((resolve, reject) => {
        api.put(path+"/delete/field", paramsApi)
        .then(response => { 
      //    console.log("Se ha eliminado"+fieldName)
          resolve(response);
        })
       .catch(error => {
         console.log("Catch:"+error)
         reject(error)
       }) 
     });
    }
  }