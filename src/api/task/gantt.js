import api from '../api.js'

let path = '/tasks'

export default {
  // si se ocupa
  addData (paramsApi) {
    return new Promise((resolve, reject) => {
      api.put(path + '/addData', JSON.stringify(paramsApi) )
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      }) 
    })
  },
  // si se ocupa
  addLink (paramsApi) {
    return new Promise((resolve, reject) => {
      api.put(path + '/addLinks', JSON.stringify(paramsApi) )
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      }) 
    })
  },
  // si se ocupa
  deleteData(paramsApi) {
    let { access_token, moduleId, id} = paramsApi
    return new Promise((resolve, reject) => {
      api.delete(path + '/' + id + '/deleteData', { params: { access_token }, data:{ moduleId }} )
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      }) 
    })
  },
  // si se ocupa
  deleteLink(paramsApi) {
    let { access_token, moduleId, id} = paramsApi
    return new Promise((resolve, reject) => {
      api.delete(path + '/' + id + '/deleteLinks', { params: { access_token }, data:{ moduleId }} )
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      }) 
    })
  },
  // si se ocupa
  updateData (paramsApi) {
    let { id } = paramsApi
    return new Promise((resolve, reject) => {
      api.put(path + '/' + id + '/updateData', JSON.stringify(paramsApi) )
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      }) 
    })
  }
}
