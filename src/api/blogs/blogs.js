import api from '../api.js'

let path = '/blogs'

export default {
  getBlogs (type) {
    return new Promise((resolve, reject) => {
      api.get(path,{ params:{type}})
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  updateBlog (paramsApi) {
    return new Promise((resolve, reject) => {
      let {
        id,
        access_token,
        title,
        content,
        meta,
        imageurl,
        type
      } = paramsApi
      var config={ 
        timeout:15000
      }
      api.put(path+'/'+id+"/update/blog",{access_token, title, content, meta, imageurl, type},config)
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      }) 
    })
  },
  insertBlog (paramsApi) {
    return new Promise((resolve, reject) => {
      api.post(path, paramsApi )
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      }) 
    })
  },
  deleteBlog (paramsApi) {
    let { access_token, id} = paramsApi
    return new Promise((resolve, reject) => {
      api.delete(path+'/'+id, {params:{access_token}} )
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      }) 
    })
  },
}