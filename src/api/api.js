import Vue from 'vue'
import axios from 'axios'
import VueSocketio from 'vue-socket.io-extended';
import io from 'socket.io-client';
import {URL_BACK} from '../varibleProd'
export default axios.create({
  baseURL: URL_BACK,
  timeout: 15000,
  headers: {
    'Content-Type': 'application/json'
  }
})

Vue.use(VueSocketio, io(URL_BACK))