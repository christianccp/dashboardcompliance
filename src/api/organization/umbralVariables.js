import api from '../api.js'

const path = '/organizations'

export default {
  
  getUmbralVars (paramsApi) {
    return new Promise((resolve, reject) => {
      let {access_token} = paramsApi
      api.get(path + '/my/vars/', { params: { access_token }})
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  setUmbralVars(paramsApi, type) {
    return new Promise((resolve, reject) => {
      api.put(path + '/my/vars/'+ type, JSON.stringify(paramsApi))
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      })
    })
  }
}