import api from '../api.js'

const path = '/organizations'

export default {
  
  getUmbralParams (paramsApi) {
    return new Promise((resolve, reject) => {
      let {access_token, moduleId} = paramsApi
      api.get(path + '/my/params?moduleId='+moduleId, { params: { access_token }})
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  setUmbralParams(paramsApi) {

    return new Promise((resolve, reject) => {
      api.put(path + '/my/params', JSON.stringify(paramsApi))
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      })
    })
  }
}
