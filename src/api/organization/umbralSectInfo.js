import api from '../api.js'

const path = '/organizations'

export default {
  
  getSectInfo (paramsApi) {
    return new Promise((resolve, reject) => {
      let access_token = paramsApi.access_token
      let moduleId = paramsApi.moduleId
      let subTypeOf =paramsApi.subTypeOf
      api.get(path + "/my/questionsInfo?moduleId="+moduleId+"&subTypeOf="+subTypeOf, { params: { access_token }})
      .then(response => {
           resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  updateInfoOrg (paramsApi) {
    return new Promise((resolve, reject) => {
      let { id } = paramsApi
      api.put(path + '/update/questionsInfo/' + id, paramsApi)
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      })
    })
  }
  
}