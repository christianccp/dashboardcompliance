import api from '../api.js'

const path = '/organizations'

export default {
  
  getInfoOrg (paramsApi) {
    return new Promise((resolve, reject) => {
      let {access_token, org_id} = paramsApi
      api.get(path + '/' + org_id, { params: { access_token }})
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  updateAreaAuthorizer (paramsApi) {
    return new Promise((resolve, reject) => {
      api.put(path + '/update/areas', paramsApi )
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  putInfoProfileOrg (paramsApi) {
    return new Promise((resolve, reject) => {
      let {access_token, org_id, name, url, zipcode, city, country} = paramsApi
      api.put(path + '/profile/' + org_id, { access_token, name, url, zipcode, city, country })
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      })
    })
  }
}