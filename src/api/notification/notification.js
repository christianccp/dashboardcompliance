import api from '../api.js'

const path = '/notifications'

export default {
    postNotification(paramsApi){
      return new Promise((resolve, reject) => {
        let {access_token, title, bodyN, link, target, typeN, idTask, optionsN} = paramsApi

        api.post(path,{ access_token: access_token, title:title, bodyN: bodyN, link:link, target: target, typeN, idTask, optionsN})
        .then(response => { resolve(response) })
        .catch(error => {
          reject(error)
        })
      })
    },
    getNotifications(paramsApi){
      return new Promise((resolve, reject) => {
        let {access_token} = paramsApi
        api.get(path, { params:{access_token: access_token} })
        .then(response => { resolve(response) })
        .catch(error => {
          reject(error)
        })
      })
    },
    deleteNotification(paramsApi) {
        let { access_token, id} = paramsApi
        return new Promise((resolve, reject) => {
          api.delete(path + '/' + id, {params:{access_token}} )
          .then(response => { resolve(response) })
          .catch(error => {
            console.log(error)
            reject(error)
          }) 
        })
    }
}