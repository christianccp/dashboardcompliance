import api from '../api.js'

const path = '/questions'

export default {

  getQuestions(paramsApi){
    return new Promise((resolve, reject) => {
      let {access_token, moduleId, typeOf} = paramsApi

      api.get(path, {params: {access_token, moduleId, typeOf}})
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  postQuestions(credentials){
    return new Promise((resolve, reject) => {
      api.post(path, credentials)
      .then(response => { resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  setResponses( paramsApi ){
    let {access_token, results, typeOf} = paramsApi
    return new Promise((resolve,reject)=>{
      api.put(path+"/set/results", {access_token, results, typeOf})
      .then( response => {resolve(response)})
      .catch(error => {
        console.log(error)
        reject(error)
      })
    })    
  },
  resetResponses( paramsApi ){
    let {access_token, typeOf} = paramsApi
    return new Promise((resolve,reject)=>{
      api.put(path+"/reset/results", {access_token, typeOf})
      .then( response => {resolve(response)})
      .catch(error => {
        reject(error)
      })
    })    
  }
}