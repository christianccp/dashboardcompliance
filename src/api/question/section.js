import api from '../api.js'

const path = '/questions'

export default {
  newSection (paramsApi){
    return new Promise((resolve, reject) => {
      api.post(path + "/new/section", paramsApi )
      .then(response => {
          resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  deleteSection (paramsApi){
    let {id, access_token} = paramsApi
    return new Promise((resolve, reject) => {
      api.delete(path + '/section/' + id, { params: { access_token }} )
      .then(response => {
          resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  updateSection (paramsApi){
    return new Promise((resolve, reject) => {
      api.put(path + '/update/section/', paramsApi )
      .then(response => {
          resolve(response) })
      .catch(error => {
        console.log(error)
        reject(error)
      })
    })
  }
}