import api from './api.js'

const path = '/auth'

// config Basic authentication request
const config = {
  auth: {
    username: '',
    password: ''
  }
}

const master_token = {"access_token": "9laxjjT3iyTJ5B3UYJaFftUmEcmVuWvv"}

export default {
  
  getToken (credentials) {
    return new Promise((resolve, reject) => {
      config.auth.username = credentials.username
      config.auth.password = credentials.password
      api.post(path, master_token, config)
      .then(response => { resolve(response) })
      .catch(error => {
    //    console.log(error)
        reject(error)
      })
    })
  }
}
