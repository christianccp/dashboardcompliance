import Vue from "vue";
import "./pollyfills";
import VueRouter from "vue-router";
import VueNotify from "vue-notifyjs";
import lang from "element-ui/lib/locale/lang/es";
import locale from "element-ui/lib/locale";
import es from "vee-validate/dist/locale/es";
import VeeValidate, { Validator } from "vee-validate";
import App from "./App.vue";
import VueTelInput from "vue-tel-input";
import IdleVue from "idle-vue";
import swal from "sweetalert2";
import VueCookies from "vue-cookies";

// VueTour
import VueTour from "vue-tour";
require("vue-tour/dist/vue-tour.css");

// Plugins
import GlobalComponents from "./plugins/globalComponents";
import GlobalDirectives from "./plugins/globalDirectives";
import SideBar from "./components/UIComponents/SidebarPlugin";
import SideBarFaq from "./components/UIComponents/SidebarFaq";
import initProgress from "./plugins/progressbar";
import { closeProgress } from "./plugins/progressbar";
import PhoneNumberValidator from "./plugins/phoneValidator";

// Store layer
import store from "./store";

// router setup
import routes from "./routes/routes";

// library imports
import "./assets/sass/paper-dashboard.scss";
import "./assets/sass/element_variables.scss";
import "./assets/sass/demo.scss";
import "vue-tel-input/dist/vue-tel-input.css";
//import 'bootstrap/dist/css/bootstrap.css'
//import 'bootstrap-vue/dist/bootstrap-vue.css'

import sidebarLinks from "./plugins/sidebarLinks";

// Bootstrap Vue Link element
import { Link } from "bootstrap-vue/es/components";
import { URL_FRONT_CLIENT } from "./varibleProd";

// plugin setup
Vue.use(VueRouter);
Vue.use(GlobalDirectives);
Vue.use(GlobalComponents);
Vue.use(VueNotify);
Vue.use(SideBar, { sidebarLinks: sidebarLinks });
Vue.use(SideBarFaq, { sidebarLinks: sidebarLinks });
Vue.use(Link);
Vue.use(VeeValidate);
Vue.use(VueTelInput);
Vue.use(VueTour);
Vue.use(VueCookies);
Validator.extend("phoneNumber", PhoneNumberValidator);

//Config Idle time
const eventsHub = new Vue();

Vue.use(IdleVue, {
  eventEmitter: eventsHub,
  idleTime: 600000
});

// confing spanish in validation messages
Validator.localize("es", es);

locale.use(lang);

// configure router
const router = new VueRouter({
  routes, // short for routes: routes
  linkActiveClass: "active",
  scrollBehavior() {
    return { x: 0, y: 0 };
  }
});

router.beforeResolve((to, from, next) => {
  if (to.meta.premiumTag) {
    if (JSON.stringify(store.state.organization.premium) === "{}") {
      if (from.name !== null) {
        Vue.prototype.$notify({
          horizontalAlign: "center",
          message: "Inicie sesión para poder usar el sistema",
          timeout: 3000,
          title: "No se ha iniciado sesión",
          type: "danger"
        });
        next({ name: "login" });
        next({
          path: "/"
        });
      } else {
        next();
      }
    } else {
      /*
      
      */

      if (store.state.organization.premium[to.meta.premiumTag]) {
        // Revisa la vigencia de VigContinuidad
        if (to.meta.premiumTag === "moduleContinuidad") {
          //Se obtiene el dato de la bd y se convierte a tipo Date
          var cadFecha = store.state.organization.premium["vigContinuidad"];
          if (!cadFecha) {
            swal({
              title: "ERROR",
              html:
                'Error al obntener vigencia de este módulo, favor de verificarlo con los administradores.<a target="_blank" href="' +
                URL_FRONT_CLIENT +
                "/buyPricing?id=" +
                id +
                '">Aqui</a>',
              allowEscapeKey: false,
              allowOutsideClick: false,
              type: "error",
              showCancelButton: false,
              confirmButtonClass: "btn btn-warning btn-fill",
              confirmButtonText: "OK"
            });
          }
          // alert(to.name);
          //bloquea url indirectasde continuidad: gantt, matrix,
          if (
            (to.name === "ganttChart-CNT" ||
              to.name === "matrix-CNT" ||
              to.name === "quizRiskParam-CNT" ||
              to.name === "editQuizParam-CNT") &&
            !store.state.organization.premium.vigenciaContinuidad
          ) {
            closeProgress(router);
            // var id = $cookies.get("token");
            swal({
              title: "Módulo con Vigencia expirada",
              html:
                "Esta ruta está desactivada, debido a que ha expirado la vigencia de este módulo.",
              allowEscapeKey: false,
              allowOutsideClick: false,
              type: "warning",
              showCancelButton: false,
              confirmButtonClass: "btn btn-warning btn-fill",
              confirmButtonText: "OK"
            });
            next(false);
          } else {
            next();
          }
        } else {
          next();
        }
      } else if (from.name) {
        closeProgress(router);
        var id = $cookies.get("token");
        swal({
          title: "Invitación a PREMIUM",
          html:
            'Si desea tener acceso a todas las características de este sistema puede adquirir una cuenta PREMIUM desde <a target="_blank" href="' +
            URL_FRONT_CLIENT +
            "/buyPricing?id=" +
            id +
            '">Aqui</a>',
          allowEscapeKey: false,
          allowOutsideClick: false,
          type: "warning",
          showCancelButton: false,
          confirmButtonClass: "btn btn-warning btn-fill",
          confirmButtonText: "OK"
        });
        next(false);
      } else {
        next({
          path: "/"
        });
      }
    }
  } else {
    next();
  }
});

initProgress(router);

/* eslint-disable no-new */
new Vue({
  el: "#app",
  store,
  render: h => h(App),
  router,
  onIdle() {
    store.dispatch("auth/logout").then(() => {
      this.$router.push({ name: "exit" });
    });
  }
}).$mount("#app");
