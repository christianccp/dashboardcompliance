import * as R from 'ramda'

export class TaskExcel {
  constructor(task = {}) {
    this.type = 'task'
    this.parent = 0
    this.text =  R.is(String, task.Tarea) ? task.Tarea: ""
    this.assigned = R.is(String, task.Responsable) ? task.Responsable : ""
    this.start_date = R.is(Date, task.Inicio) ? task.Inicio : new Date()
    this.end_date = R.is(Date, task.Fin) ? task.Fin : new Date()
    this.end_date.setHours(23,59)
    this.comments = R.is(String, task.Comentarios) ? task.Comentarios : ""
    this.predecessors = R.is(String, task.Predecesor) ? task.Predecesor : ""
    this.originType = R.is(String, task.originType) ? task.originType : ""
    this.processOwner = R.is(String, task.processOwner) ? task.processOwner : ""
    this.progress = R.is(Number, task.Progreso) ? task.Progreso : 0
  }
}
