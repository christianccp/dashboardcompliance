import * as R from 'ramda'

export class Catalog {
  constructor(catalog = {}) {
    this.value = R.is(Number, catalog.value) ? catalog.value : -1
    this.label = R.is(String, catalog.label) ? catalog.label : "" 
  }
}

export class Subrisk {
  constructor(subrisk = {}) {
    this._id =  R.is(String, subrisk._id) ? subrisk._id: ""
    this.organization_id = R.is(String, subrisk.organization_id) ? subrisk.organization_id : ""
    this.moduleId = R.is(String, subrisk.moduleId) ? subrisk.moduleId : ""
    this.active = R.is(Boolean, subrisk.active) ? subrisk.active : ""
    this.title = R.is(String, subrisk.title) ? subrisk.title : ""
    this.description = R.is(String, subrisk.description) ? subrisk.description : ""
    this.sanction = R.is(String, subrisk.sanction) ? subrisk.sanction : ""
    this.impactAvg = new Catalog(subrisk.impactAvg)
    this.levelInhAvg = new Catalog(subrisk.levelInhAvg)
    this.probabilityAvg = new Catalog(subrisk.probabilityAvg)
    this.proceder = new Catalog(subrisk.proceder)
    this.params = R.is(Array, subrisk.params) ? subrisk.params : []
    this.levelResidualAvg = new Catalog(subrisk.levelResidualAvg)
    this.quizAssigned =  R.is(String, subrisk.quizAssigned) ? subrisk.quizAssigned: ""
    
  }
  
  // funcion que esta listo para enviar al Servicio API
    loadData() {
      return {
        _id: this._id, 
        organization_id: this.organization_id,
        moduleId: this.moduleId,
        active: this.active,
        title: this.title,
        description: this.description,
        sanction: this.sanction,
        impactAvg: this.impactAvg,
        levelInhAvg: this.levelInhAvg,
        probabilityAvg: this.probabilityAvg,
        proceder: this.proceder,
        params: this.params,
        levelResidualAvg: this.levelResidualAvg
      }
  }
}