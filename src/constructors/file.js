import * as R from 'ramda'

export class File {
  constructor(file = {}) {
    this.id = file.id
    this.dateUpdate = R.is(String, file.dateUpdate) ? file.dateUpdate : ""
    this.folder =  R.is(String, file.folder) ? file.folder: ""
    this.fileName = R.is(String, file.fileName) ? file.fileName : ""
    this.fileUrl = R.is(String, file.fileUrl) ? file.fileUrl : ""
    this.key = R.is(String, file.key) ? file.key : ""
    this.version = R.is(Array, file.version) ? file.version : []
    this.logUsersAccess = R.is(Array, file.logUsersAccess) ? file.logUsersAccess : []
    this.status = R.is(String, file.status) ? file.status : ""
    this.validity = file.validity !== null && file.validity !== undefined ? new Date(file.validity.split('T')[0]).toLocaleDateString() : "---"
    this.areaInvolved = R.is(Array, file.areaInvolved) ? file.areaInvolved : []    
  }
}
