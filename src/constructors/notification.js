import * as R from 'ramda'

export class Notification {
  constructor(notification = {}) {
    this.id = notification._id
    this.title =  R.is(String, notification.title) ? notification.title: ""
    this.bodyN = R.is(String, notification.bodyN) ? notification.bodyN : ""
    this.link = R.is(String, notification.link) ? notification.link : ""
    this.target = R.is(Array, notification.target) ? notification.target : []
    this.organization_id = R.is(String, notification.organization_id) ? notification.organization_id : ""
    this.createdAt = R.is(String, notification.createdAt) ? notification.createdAt : ""
    this.typeN = R.is(String, notification.typeN) ? notification.typeN : ""
    this.optionsN = R.is(Object, notification.optionsN) ? notification.optionsN : {}
  }
}
