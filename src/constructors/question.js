import * as R from 'ramda'

export class Result {
  constructor(result = {}) {
    this.subrisk_id =  R.is(String, result.subrisk_id) ? result.subrisk_id: ""
    this.subriskTitle = R.is(String, result.subriskTitle) ? result.subriskTitle : ""
    this.value = R.is(Number, result.value) ? result.value : -1
  }
}
export class LinkedSubrisk {
  constructor(linkedSubrisk = {}) {
    this.subrisk_id =  R.is(String, result.subrisk_id) ? result.subrisk_id: ""
    this.subriskTitle = R.is(String, result.subriskTitle) ? result.subriskTitle : ""
    this.value = R.is(Number, result.value) ? result.value : -1
  }
}
export class Question {
  constructor(question = {}) {
    this.id =  R.is(String, question.id) ? question.id: ""
    this.title = R.is(String, question.title) ? question.title : ""
    this.weight = R.is(Number, question.weight) ? question.weight : ""
    this.moduleId = R.is(String, question.moduleId) ? question.moduleId : ""
    this.typeOf = R.is(String, question.typeOf) ? question.typeOf : ""
    this.taskDescription = R.is(String, question.taskDescription) ? question.taskDescription : ""
    this.questionNumber = R.is(Number, question.questionNumber) ? question.questionNumber : ""
    this.organization_id = R.is(String, question.organization_id) ? question.organization_id : ""
    
    if(this.typeOf === "questionnaire1"){
      this.results = new Array
      this.isCustomQuestion = R.is(Boolean, question.isCustomQuestion) ? question.isCustomQuestion : false
      
      question.results.map((result)=>{
        this.results.push(new Result(result))
      })
    }else{
      this.section_id = R.is(String, question.section_id) ? question.section_id : ""
      this.result = R.is(Number, question.result) ? question.result : "-1"
      this.linkedSubrisks = R.is(Array, question.linkedSubrisks) ? question.linkedSubrisks : []
      this.impactVars = R.is(Array, question.impactVars) ? question.impactVars : []
    }
  }
}