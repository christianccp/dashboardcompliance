import * as R from 'ramda'

export class User {
  constructor(user = {}) {
      this.name = R.is(String, user.name) ? user.name: ""
      this.picture = R.is(String, user.picture) ? user.picture: ""
      this.phone = R.is(Number, user.phone) ? user.phone: ""
      this.mobile = R.is(Number, user.mobile) ? user.mobile: ""
      this._id = R.is(String, user.id) ? user.id: "token_org"
      this.ocupation = R.is(String, user.ocupation) ? user.ocupation: ""
      this.role = R.is(String, user.role) ? user.role: ""
      this.roleDocMgt = R.is(String, user.roleDocMgt) ? user.roleDocMgt: ""
      this.authorizer = R.is(Boolean, user.authorizer) ? user.authorizer: false
      this.flags = R.is(Object, user.flags) ? user.flags: {hideMessageEditSubrisk: false, hideTutorialTour: false}
      this.isAuditor = R.is(Boolean, user.isAuditor) ? user.isAuditor: false
  }
}