import * as R from 'ramda'

export class Data {
  constructor(ganttChartData = {}) {
    this.id =  R.is(Number, ganttChartData.id) ? ganttChartData.id: 0
    this.text =  R.is(String, ganttChartData.text) ? ganttChartData.text: ""
    this.start_date_orig = R.is(String, ganttChartData.start_date_orig) ? ganttChartData.start_date_orig: R.is(String, ganttChartData.start_date) ? ganttChartData.start_date: new Date().toString()
    this.end_date_orig = R.is(String, ganttChartData.end_date_orig) ? ganttChartData.end_date_orig: R.is(String, ganttChartData.end_date) ? ganttChartData.end_date: new Date().toString()       
    this.start_date = convertDate(this.start_date_orig)
    this.end_date = convertDate(this.end_date_orig)
    this.progress =  R.is(Number, ganttChartData.progress) ? ganttChartData.progress: 0
    this.assigned =  R.is(String, ganttChartData.assigned) ? ganttChartData.assigned: ""
    this.comments =  R.is(String, ganttChartData.comments) ? ganttChartData.comments: ""
    this.color =  R.is(String, ganttChartData.color) ? ganttChartData.color: ""
    this.open = R.is(Boolean, ganttChartData.open) ? ganttChartData.open : true
    this.type = R.is(String, ganttChartData.type) ? ganttChartData.type : ""
    this.parent = R.is(Number, ganttChartData.parent) ? ganttChartData.parent : 0
    this.avance =  pregress(ganttChartData.progress)
    this.priority = R.is(Number, ganttChartData.priority) ? ganttChartData.priority : 0
    this.responsibleArea = R.is(String, ganttChartData.responsibleArea) ? ganttChartData.responsibleArea : ""
    this.measureIndicator = R.is(String, ganttChartData.measureIndicator) ? ganttChartData.measureIndicator : ""
    this.controlFrecuency = R.is(String, ganttChartData.controlFrecuency) ? ganttChartData.controlFrecuency : " "
    this.originType = R.is(String, ganttChartData.originType) ? ganttChartData.originType : ""
    this.processOwner = R.is(String, ganttChartData.processOwner) ? ganttChartData.processOwner : ""
    this.evaluationResult = R.is(String, ganttChartData.evaluationResult) ? ganttChartData.evaluationResult : ""
    this.proceder = R.is(String, ganttChartData.proceder) ? ganttChartData.proceder : " "
    this.linkedSubrisks = R.is(Array, ganttChartData.linkedSubrisks) ? ganttChartData.linkedSubrisks : []
    this.audit=R.is(Object,ganttChartData.audit) ? ganttChartData.audit : {}
    this.next_day_monitory= R.is(String, ganttChartData.next_day_monitory) ? ganttChartData.next_day_monitory : ""
    this.newFields= R.is(Array, ganttChartData.newFields) ? ganttChartData.newFields: [] 
    this.filesIdDocument = R.is(Array, ganttChartData.filesIdDocument) ? ganttChartData.filesIdDocument: []
    this.stepAudit = R.is(Number, ganttChartData.stepAudit) ? ganttChartData.stepAudit : 1
    this.observations= R.is(String, ganttChartData.observations) ? ganttChartData.observations : ""
    this.satisfy= R.is(String, ganttChartData.satisfy) ? ganttChartData.satisfy : null
  }
}

export class Link {
  constructor(ganttChartLink = {}) {
    this.id =  R.is(Number, ganttChartLink.id) ? ganttChartLink.id: 0
    this.source =  R.is(Number, ganttChartLink.source) ? ganttChartLink.source: 0
    this.target =  R.is(Number, ganttChartLink.target) ? ganttChartLink.target: 0
    this.type =  R.is(Number, ganttChartLink.type) ? ganttChartLink.type: 0
  }
}

export class Gantt {
  constructor(ganttChartTask = {}) {
    this.id =  R.is(String, ganttChartTask._id) ? ganttChartTask._id: ""
    this.data = new Array
    this.links = new Array
    this.organization_id =  R.is(String, ganttChartTask.organization_id) ? ganttChartTask.organization_id: ""

    ganttChartTask.tasks.data.map((ganttChartData)=>{
      this.data.push(new Data(ganttChartData))
    })

    ganttChartTask.tasks.links.map((ganttChartLink)=>{
      this.links.push(new Link(ganttChartLink))
    })

  }
  

  // funcion que esta listo para enviar al Servicio API
    loadData() {
      return {
        _id: this._id, 
        data: this.data,
        link: this.link,
        organization_id: this.organization_id,
        moduleId: this.moduleId
      }
  }
}
// TODO: Cachar cuando una fecha no es valida, regresar 0.
const convertDate = (dateIn) => {
  if (typeof dateIn === 'undefined'){
    return undefined
  }
  let date = new Date (dateIn)
  if(date.toString() == 'Invalid Date'){
    date = new Date()
  }
  var day = date.getDate()
  var month = date.getMonth() + 1
  var year = date.getFullYear()
  let dateOut = day + '-' + month + '-' + year 
  return dateOut
}
const pregress = (progressTask) => {
  return progressTask * 100
}