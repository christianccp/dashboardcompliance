import * as R from 'ramda'

export function fixDatesFromExcel (Inicio, Fin) {
    var date
    var dateDay
    var dateHour
    if(R.is(Number, Inicio)){
        Inicio = new Date((Inicio - (25567 + 1))*86400*1000)
      }else{
        if( Inicio  != undefined && Inicio.length == 16 ){
          date = Inicio.split(" ")
          if(date.length == 2){
            dateDay = date[0].split("-")
            dateHour = date[1].split(":")
            Inicio = new Date(dateDay[2], dateDay[1]-1, dateDay[0], dateHour[0], dateHour[1],0,0)
          }       
          
        }else{
          Inicio = new Date()
        }
      }

      while(!gantt.isWorkTime(Inicio)){
        Inicio = new Date(Inicio.getTime()+ 86400000)
      }

      if(R.is(Number, Fin)){
        Fin = new Date((Fin - (25567 + 1))*86400*1000)
      }else{
        if( Fin  != undefined && Fin.length == 16 ){
          date = Fin.split(" ")
          if(date.length == 2){
            dateDay = date[0].split("-")
            dateHour = date[1].split(":")
            Fin = new Date(dateDay[2], dateDay[1] -1, dateDay[0], dateHour[0], dateHour[1],0,0)
          }                      
        }else{
          Fin = new Date(Inicio.getTime()+ 86400000)
        }
      }

      while(!gantt.isWorkTime(Fin)){
        Fin = new Date(Fin.getTime()+ 86400000)
      }

      if(Fin.getTime() - Inicio.getTime() <= 0){
        Fin = new Date(Inicio.getTime()+86400000)
      }
      while(!gantt.isWorkTime(Fin)){
        Fin = new Date(Fin.getTime()+ 86400000)
      }

      return {Inicio, Fin}
}

export function getValidDate () {
    var Inicio, Fin
    Inicio = new Date()
    while(!gantt.isWorkTime(Inicio)){
        Inicio = new Date(Inicio.getTime()+ 86400000)
    }
    Fin = new Date(Inicio.getTime()+ 86400000)
    while(!gantt.isWorkTime(Fin)){
        Fin = new Date(Fin.getTime()+ 86400000)
    }
    return {Inicio, Fin}
}

export function getValidDateMultipleTask () {
  var Inicio, Fin
  Inicio = new Date()
  while(Inicio.getDay() == 6 || Inicio.getDay() == 0){
      Inicio = new Date(Inicio.getTime()+ 86400000)
  }
  Fin = new Date(Inicio.getTime()+ 86400000)
  while(Fin.getDay() == 6 || Fin.getDay() == 0){
      Fin = new Date(Fin.getTime()+ 86400000)
  }
  return {Inicio, Fin}
}