import * as R from 'ramda'

/**
 * 
 * @param {String} propertyName 
 * @param {Object} object 
 */
export function sortDesc (propertyName, object) {
  return R.sort(R.descend(R.prop(propertyName)),object )
}


/**
 * 
 * @param {String} queryValue Valor a buscar
 * @param {Array} catalog 
 * @return 
 */
export function getLabelLevel (queryValue, catalog) {

  let defaulLabel = 'No definido'
  let label = defaulLabel
  let property = 'value'

  if (typeof catalog === "undefined" ) {
    return defaulLabel
  }

  let catalogSorted = sortDesc(property, catalog)

  catalogSorted.forEach((item) => {
    if (queryValue >= item.value &&  label === defaulLabel ) {
      label = item.label
    }
  })
  return label
}

/**
 * 
 * @param {String} queryValue 
 * @param {Array} catalog 
 */
export function getLabel (queryValue, catalog) {
  let result = catalog.find(obj => obj.value == queryValue)
  return (result) ?  result.label : 'No definido'
} 

/**
 * 
 * @param {String} text
 */
export function getAbbrev (text) {
  var words = text.split(' ')
  if(words.length > 1){
    return words.map(word=>{ return word[0]}).join('').toUpperCase()
  }else{
    return text.substring(0,3).toUpperCase()
  }
}  