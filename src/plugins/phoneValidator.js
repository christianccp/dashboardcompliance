// import google phone validator
import PhoneNumber from 'awesome-phonenumber'

// config Google phone validator with VeeValidate lib
const PhoneNumberValidator = {
  getMessage: field => 'El '+ field + ' no es número válido.',
  validate (value) {
    return new Promise(resolve => {
      let phone = new PhoneNumber(value);
      resolve({ valid: phone.isValid() })
    })
  }
}


export default PhoneNumberValidator