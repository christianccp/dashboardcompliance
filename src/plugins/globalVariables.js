
const SER_RESP = {
  UNAUTHORIZED: 401,
  NOT_FOUND: 400,
  SUCCESS: 201
}

const RC = {
  NOT_VALID_SESSION: {
    CODE: 1001,
    MSG:'Correo electrónico o contraseña erroneos',
    TITLE: 'Inicio de sesión no valido'
  },
  NOT_SVR_AVAILABLE: {
    CODE: 1002,
    MSG:'Servicio no disponible',
    TITLE: 'Error Interno'
  },
  BAD_REQUEST: {
    CODE: 1400,
    MSG:'Petición erronea',
    TITLE: 'Error Interno'
  },
} 

export {SER_RESP, RC}