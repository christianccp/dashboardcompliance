import store from "../store";

export default [
  {
    name: "Tablero Principal",
    icon: "nc-icon nc-bank",
    path: "/admin/overview",
    premiumTag: "someModuleObtained"
  },
  {
    name: "Diagnóstico",
    icon: "nc-icon nc-bullet-list-67",
    children: [
      {
        name: "Anticorrupción",
        path: "/autoevaluation/anticorruption"
      }
      /*{
        name: 'Subcontratación',
        path: '#'
      },
      {
        name: 'Datos Personales',
        path: '#'
      },
      {
        name: 'Lavado de dinero',
        path: '#'
      }*/
    ]
  },
  /*{
    name: 'Diagnóstico',
    icon: 'nc-icon nc-bullet-list-67',
    children: [
      
      {
        name: 'Anticorrupción',
        path: '/anticorruption/actionplan',
      },
      {
        name: 'Subcontratación',
        path: '#',
      },
      {
        name: 'Datos Personales',
        path: '#',
      },
      {
        name: 'Lavado de dinero',
        path: '#',
      },
    ]
  },*/
  {
    name: "Anticorrupción",
    icon: "nc-icon nc-circle-10",
    premiumTag: "moduleAnticorruption",
    children: [
      {
        name: "Gestión de Riesgo",
        icon: "el-icon-menu",
        path: "/anticorruption",
        children: [
          {
            name: "Identificación de Riesgos",
            icon: "el-icon-aim",
            path: "/anticorruption/identify",
            children: [
              {
                name: "Tabla de riesgos",
                path: "/anticorruption/identify/risk"
              },
              {
                name: "Evaluación de riesgo residual",
                path: "/anticorruption/identify/ques"
              }
            ]
          },
          {
            name: "Evaluación de Controles",
            path: "/anticorruption/secteval",
            icon: "el-icon-set-up"
          },
          {
            name: "Evaluación y Tratamiento de Riesgo",
            icon: "el-icon-notebook-2",
            path: "/anticorruption/evaluate",
            children: [
              {
                name: "Priorización en el tratamiento de riesgos",
                path: "/anticorruption/evaluate/evaluaterisk"
              },
              {
                name: "Mapa de Riesgos",
                path: "/anticorruption/evaluate/riskMap"
              }
            ]
          }
        ]
      },
      {
        name: "Planes de acción",
        path: "/anticorruption/actionplan"
      },
      {
        name: "Monitoreo",
        path: "/anticorruption/Monitoreo"
      },
      {
        name: "Revisión y auditoría",
        path: "/anticorruption/auditoria"
      }
    ]
  },

  {
    name: "Servicios especializados",
    icon: "fa fa-balance-scale",
    premiumTag: "modulePenal",
    children: [
      {
        name: "Gestión de Riesgo",
        icon: "el-icon-menu",
        path: "/penal",
        children: [
          {
            name: "Identificación de Riesgos",
            icon: "el-icon-aim",
            path: "/penal/identify",
            children: [
              {
                name: "Tabla de riesgos",
                path: "/penal/identify/risk"
              },
              {
                name: "Evaluación de riesgo residual",
                path: "/penal/identify/ques"
              }
            ]
          },
          {
            name: "Evaluación de Controles",
            path: "/penal/secteval",
            icon: "el-icon-set-up"
          },
          {
            name: "Evaluación y Tratamiento de Riesgo",
            icon: "el-icon-notebook-2",
            path: "/penal/evaluate",
            children: [
              {
                name: "Priorización en el tratamiento de riesgos",
                path: "/penal/evaluate/evaluaterisk"
              },
              {
                name: "Mapa de Riesgos",
                path: "/penal/evaluate/riskMap"
              }
            ]
          }
        ]
      },
      {
        name: "Planes de acción",
        path: "/penal/actionplan"
      },
      {
        name: "Monitoreo",
        path: "/penal/Monitoreo"
      },
      {
        name: "Revisión y auditoría",
        path: "/penal/auditoria"
      }
    ]
  },

  {
    name: "Continuidad de negocio Covid-19",
    icon: "fa fa-chain-broken",
    premiumTag: "moduleContinuidad",
    children: [
      {
        name: "Gestión de Riesgo",
        icon: "el-icon-menu",
        path: "/continuidad",
        children: [
          {
            name: "Identificación de Riesgos",
            icon: "el-icon-aim",
            path: "/continuidad/identify",
            children: [
              {
                name: "Tabla de riesgos",
                path: "/continuidad/identify/risk"
              },
              {
                name: "Evaluación de riesgo residual",
                path: "/continuidad/identify/ques"
              }
            ]
          },
          {
            name: "Evaluación de Controles",
            path: "/continuidad/secteval",
            icon: "el-icon-set-up"
          },
          {
            name: "Evaluación y Tratamiento de Riesgo",
            icon: "el-icon-notebook-2",
            path: "/continuidad/evaluate",
            children: [
              {
                name: "Priorización en el tratamiento de riesgos",
                path: "/continuidad/evaluate/evaluaterisk"
              },
              {
                name: "Mapa de Riesgos",
                path: "/continuidad/evaluate/riskMap"
              }
            ]
          }
        ]
      },
      {
        name: "Planes de acción",
        path: "/continuidad/actionplan"
      },
      {
        name: "Monitoreo",
        path: "/continuidad/Monitoreo"
      },
      {
        name: "Revisión y auditoría",
        path: "/continuidad/auditoria"
      }
    ]
  },
  {
    name: "Laboral",
    path: "#",
    icon: "fa fa-handshake-o",
    children: [
      {
        name: "Planes de acción"
        //path: '#'
      },
      {
        name: "Revisión y auditoría"
        //path: '#'
      },
      {
        name: "Comunicación"
        //path: '#'
      },
      {
        name: "E-learning"
        //path: '#'
      }
    ]
  },
  {
    name: "Datos Personales",
    path: "#",
    icon: "fa fa-newspaper-o",
    children: [
      {
        name: "Gestión de Riesgo"
        /*      icon: 'el-icon-menu',
        path: '/personaldata',
        children:[
          {
            
            name: 'Identificación de Riesgos',
            path: '/personaldata/risk'
          },
          {
            name: 'Evaluación de Controles',
            path: '/personaldata/secteval'
          }
        ] */
      }
      /*       {
        name: 'Planes de acción',
        path: '/personaldata/actionplan'
      },
      {
        name: 'Monitoreo',
        path: '/personaldata/Monitoreo'
      },
      {
        name: 'Revisión y auditoría',
        path: '/personaldata/auditoria'
      } */
    ]
  },

  {
    name: "Lavado de dinero",
    icon: "fa fa-ban",
    premiumTag: "moduleMoney",
    children: [
      {
        name: "Gestión de Riesgo",
        icon: "el-icon-menu",
        path: "/money",
        children: [
          {
            name: "Identificación de Riesgos",
            icon: "el-icon-aim",
            path: "/money/identify",
            children: [
              {
                name: "Tabla de riesgos",
                path: "/money/identify/risk"
              },
              {
                name: "Evaluación de riesgo residual",
                path: "/money/identify/ques"
              }
            ]
          },
          {
            name: "Evaluación de Controles",
            path: "/money/secteval",
            icon: "el-icon-set-up"
          },
          {
            name: "Evaluación y Tratamiento de Riesgo",
            icon: "el-icon-notebook-2",
            path: "/money/evaluate",
            children: [
              {
                name: "Priorización en el tratamiento de riesgos",
                path: "/money/evaluate/evaluaterisk"
              },
              {
                name: "Mapa de Riesgos",
                path: "/money/evaluate/riskMap"
              }
            ]
          }
        ]
      },
      {
        name: "Planes de acción",
        path: "/money/actionplan"
      },
      {
        name: "Monitoreo",
        path: "/money/Monitoreo"
      },
      {
        name: "Revisión y auditoría",
        path: "/money/auditoria"
      }
    ]
  },

  {
    name: "Sistema PAD",
    icon: "fa fa-tachometer",
    children: [
      /*{
        name: 'Accesibilidad',
        path: '*'
      },*/
      {
        name: "Administración",
        path: "/padSystem/administration",
        premiumTag: "someModuleObtained"
      },
      {
        name: "Opciones",
        path: "/padSystem/options",
        premiumTag: "someModuleObtained"
      },
      {
        name: "Gestor documental",
        path: "/padSystem/fileManagerMenu",
        premiumTag: "serviceDocMgt"
      },
      {
        name: "Monitor",
        path: "/padSystem/monitor",
        premiumTag: "someModuleObtained"
      }
      /*{
        name: 'Planes de trabajo',
        path: '*'
      },
      {
        name: 'Gestión documental',
        path: '*'
      },
      {
        name: 'Gestión incidencias',
        path: '*'
      },
      {
        name: 'Gestión operativa',
        path: '*'
      }*/
    ]
  }
];
