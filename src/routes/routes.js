import DashboardLayout from "../components/Dashboard/Layout/DashboardLayout.vue";
// GeneralViews
import NotFound from "../components/GeneralViews/NotFoundPage.vue";

// Dashboard pages
const Overview = () =>
  import(/* webpackChunkName: "dashboard" */ "src/components/Dashboard/MainBoard/Overview.vue");
const AnticorruptionBoard = () =>
  import(/* webpackChunkName: "dashboard" */ "src/components/Dashboard/MainBoard/AnticorruptionBoard.vue");

// Perfil de usuario
import UserProfile from "src/components/Dashboard/UserProfile/UserProfile.vue";

// Pages
import Login from "src/components/Pages/Login.vue";
import Register from "src/components/Pages/Register.vue";
import Lock from "src/components/Pages/Lock.vue";
import Account from "src/components/Pages/Account.vue";
import Password from "src/components/Pages/Password.vue";
import Recoverymail from "src/components/Pages/Recoverymail.vue";

import store from "./../store";

// Autoevaluation Menu
const RiskMap = () =>
  import(/* webpackChunkName: "dashboard" */ "src/components/Dashboard/Anticorruption/RiskMap.vue");
const MenuDiagnostic = () =>
  import(/* webpackChunkName: "diagnostic" */ "src/components/Dashboard/Autoevaluation/MenuDiagnostic.vue");

// Anticorruption Menu
const MenuRisk = () =>
  import(/* webpackChunkName: "identifyrisk" */ "src/components/Dashboard/Anticorruption/MenuIdentifyRisk.vue");
const MenuControlEvaluation = () =>
  import(/* webpackChunkName: "controlevaluation" */ "src/components/Dashboard/Anticorruption/MenuControlEvaluation.vue");
const Risk = () =>
  import(/* webpackChunkName: "identifyrisk" */ "src/components/Dashboard/Anticorruption/IdentifyRisk.vue");
const EvaluateRisk = () =>
  import(/* webpackChunkName: "controlevaluation" */ "src/components/Dashboard/Anticorruption/EvaluateRisk.vue");
const Ques = () =>
  import(/* webpackChunkName: "identifyrisk" */ "src/components/Dashboard/Anticorruption/Evaluation/Ques.vue");
const GraphQas = () =>
  import(/* webpackChunkName: "identifyrisk" */ "src/components/Dashboard/Anticorruption/Evaluation/GraphQas.vue");
const SectEval = () =>
  import(/* webpackChunkName: "controlevaluation" */ "src/components/Dashboard/Anticorruption/Evaluation/SectEval.vue");
const WorkPlan = () =>
  import(/* webpackChunkName: "actionplan" */ "src/components/Dashboard/Anticorruption/WorkPlan.vue");
const Monitoreo = () =>
  import(/* webpackChunkName: "monitoreo" */ "src/components/Dashboard/Anticorruption/Monitoreo.vue");
const ActionPlan = () =>
  import(/* webpackChunkName: "actionplan" */ "src/components/Dashboard/Anticorruption/ActionPlan.vue");
const Auditoria = () =>
  import(/* webpackChunkName: "auditorias" */ "src/components/Dashboard/Anticorruption/Auditoria.vue");
//Matriz de Riesgos
const RiskMatrix = () =>
  import(/* webpackChunkName: "identifyrisk" */ "src/components/Dashboard/MatrizRiesgos/Matrix.vue");
const QuizRisk = () =>
  import(/* webpackChunkName: "controlevaluation" */ "src/components/Dashboard/QuizRisk/quizRisk.vue");
const GanttChart = () =>
  import(/* webpackChunkName: "actionplan" */ "src/components/Dashboard/GanttChart/ganttChart.vue");

//Sistema PAD
const Administration = () =>
  import(/* webpackChunkName: "blog" */ "src/components/Dashboard/PADSystem/Administration.vue");
const Options = () =>
  import(/* webpackChunkName: "options" */ "src/components/Dashboard/PADSystem/Options.vue");
const BlogTable = () =>
  import(/* webpackChunkName: "blog" */ "src/components/Dashboard/PADSystem/BlogTable.vue");
const EditBlog = () =>
  import(/* webpaczkChunkName: "blog" */ "src/components/Dashboard/PADSystem/EditBlog.vue");
const FileManagerMenu = () =>
  import(/* webpaczkChunkName: "filemanager" */ "src/components/Dashboard/PADSystem/FileManagerMenu.vue");
const FileManager = () =>
  import(/* webpaczkChunkName: "filemanager" */ "src/components/Dashboard/PADSystem/FileManager.vue");
const RolesFM = () =>
  import(/* webpaczkChunkName: "filemanager" */ "src/components/Dashboard/PADSystem/RolesFM.vue");
const Monitor = () => import("src/components/Dashboard/PADSystem/Monitor.vue");

//MODULO PENAL
const EditQuizParam = () =>
  import("src/components/Dashboard/Penal/EditQuesParam");
const SectEvalPenalOr = () =>
  import(/* webpackChunkName: "controlevaluation" */ "src/components/Dashboard/Penal/SectEvalPenal.vue");
const QuizRiskPenal = () =>
  import(/* webpackChunkName: "controlevaluation" */ "src/components/Dashboard/Penal/quizRiskPenal.vue");
import VueNotify from "vue-notifyjs";
import Vue from "vue";
import { URL_FRONT_CLIENT } from "../varibleProd";
Vue.use(VueNotify);
var verifySession = (to, from, next) => {
  const token = $cookies.get("token");
  if (token === null) {
    Vue.prototype.$notify({
      horizontalAlign: "center",
      message: "Inicie sesión para poder usar el sistema",
      timeout: 3000,
      title: "No se ha iniciado sesión",
      type: "danger"
    });
    next("/");
  } else {
    next();
  }
};

// Calendar
import Calendar from "src/components/Dashboard/Views/Calendar/CalendarRoute.vue";

let autoevaluationMenu = {
  path: "/autoevaluation",
  component: DashboardLayout,
  beforeEnter: verifySession,
  redirect: "/autoevaluation/MenuDiagnostic",
  children: [
    {
      path: "anticorruption",
      name: "anticorruption",
      component: MenuDiagnostic,
      beforeEnter: verifySession
    },
    {
      path: "anticorruption/quiz",
      name: "quiz",
      component: QuizRisk
    }
  ]
};

let userProfile = {
  path: "/UserProfile",
  component: DashboardLayout,
  beforeEnter: verifySession,
  redirect: "/UserProfile/UserProfile",
  children: [
    {
      path: "/UserProfile/UserProfile",
      name: "userProfile",
      component: UserProfile,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "serviceUserProfile"
      }
    }
  ]
};

let padSystemMenu = {
  path: "/padSystem",
  component: DashboardLayout,
  beforeEnter: verifySession,
  redirect: "/padSystem/administration",
  children: [
    {
      path: "administration",
      name: "administration",
      component: Administration,
      beforeEnter: verifySession
    },
    {
      path: "options",
      name: "options",
      component: Options,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "someModuleObtained"
      }
    },
    {
      path: "editBlog",
      name: "editBlog",
      component: EditBlog,
      beforeEnter: verifySession
    },
    {
      path: "fileManagerMenu",
      name: "fileManagerMenu",
      component: FileManagerMenu,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "serviceDocMgt"
      }
    },
    {
      path: "fileManager/:id",
      name: "fileManager",
      component: FileManager,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "serviceDocMgt"
      }
    },
    {
      path: "rolesManager",
      name: "rolesFM",
      component: RolesFM,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "serviceDocMgt"
      }
    },
    {
      path: "monitor",
      name: "monitor",
      component: Monitor,
      beforeEnter: verifySession
    }
  ]
};

let padSystemAdmin = {
  path: "/padSystem/administration",
  component: DashboardLayout,
  beforeEnter: verifySession,
  redirect: "/padSystem/administration",
  children: [
    {
      path: "blogTable",
      name: "blogTable",
      component: BlogTable,
      beforeEnter: verifySession
    }
  ]
};

let anticorruptionMenu = {
  path: "/anticorruption",
  component: DashboardLayout,
  beforeEnter: verifySession,
  redirect: "/anticorruption/identify/risk",
  children: [
    {
      path: "identify/risk",
      name: "risk",
      component: Risk,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleAnticorruption"
      }
    },
    {
      path: "evaluate/riskMap",
      name: "riskMap",
      component: RiskMap,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleAnticorruption"
      }
    },
    {
      path: "Monitoreo",
      name: "Monitoreo",
      component: Monitoreo,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleAnticorruption"
      }
    },
    {
      path: "identify/ques",
      name: "ques",
      component: Ques,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleAnticorruption"
      }
    },
    {
      path: "identify/graphqas",
      name: "graphqas",
      component: GraphQas,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleAnticorruption"
      }
    },
    {
      path: "secteval",
      name: "secteval",
      component: SectEval,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleAnticorruption"
      }
    },
    {
      path: "evaluate/evaluaterisk",
      name: "evaluaterisk",
      component: EvaluateRisk,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleAnticorruption"
      }
    },
    {
      path: "workplan",
      name: "workplan",
      component: WorkPlan,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleAnticorruption"
      }
    },
    {
      path: "calendar",
      name: "calendar",
      component: Calendar,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleAnticorruption"
      }
    },
    {
      path: "matrix",
      name: "matrix",
      component: RiskMatrix,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleAnticorruption"
      }
    },
    {
      path: "secteval/quizRisk",
      name: "quizRisk",
      component: QuizRisk,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleAnticorruption"
      }
    },
    {
      path: "ganttChart",
      name: "ganttChart",
      component: GanttChart,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleAnticorruption"
      }
    },
    {
      path: "actionplan",
      name: "actionplan",
      component: ActionPlan,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleAnticorruption"
      }
    },
    {
      path: "auditoria",
      name: "auditoria",
      component: Auditoria,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleAnticorruption"
      }
    }
  ]
};

let personaldataMenu = {
  path: "/personaldata",
  component: DashboardLayout,
  redirect: "/personaldata/risk",
  children: [
    {
      path: "risk",
      name: "risk-PD",
      component: Risk
    },
    {
      path: "riskMap",
      name: "riskMap-PD",
      component: RiskMap
    },
    {
      path: "Monitoreo",
      name: "Monitoreo-PD",
      component: Monitoreo
    },
    {
      path: "ques",
      name: "ques-PD",
      component: Ques
    },
    {
      path: "graphqas",
      name: "graphqas-PD",
      component: GraphQas
    },
    {
      path: "secteval",
      name: "secteval-PD",
      component: SectEval
    },
    {
      path: "evaluaterisk",
      name: "evaluaterisk-PD",
      component: EvaluateRisk
    },
    {
      path: "workplan",
      name: "workplan-PD",
      component: WorkPlan
    },
    {
      path: "calendar",
      name: "calendar-PD",
      component: Calendar
    },
    {
      path: "matrix",
      name: "matrix-PD",
      component: RiskMatrix
    },
    {
      path: "quizRisk",
      name: "quizRisk-PD",
      component: QuizRisk
    },
    {
      path: "ganttChart",
      name: "ganttChart-PD",
      component: GanttChart
    },
    {
      path: "actionplan",
      name: "actionplan-PD",
      component: ActionPlan
    },
    {
      path: "auditoria",
      name: "auditoria-PD",
      component: Auditoria
    }
  ]
};

let penalMenu = {
  path: "/penal",
  component: DashboardLayout,
  beforeEnter: verifySession,
  redirect: "/penal/identify/risk",
  children: [
    {
      path: "identify/risk",
      name: "risk-PNL",
      component: Risk,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "modulePenal"
      }
    },
    {
      path: "evaluate/riskMap",
      name: "riskMap-PNL",
      component: RiskMap,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "modulePenal"
      }
    },
    {
      path: "Monitoreo",
      name: "Monitoreo-PNL",
      component: Monitoreo,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "modulePenal"
      }
    },
    {
      path: "identify/ques",
      name: "ques-PNL",
      component: Ques,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "modulePenal"
      }
    },
    {
      path: "identify/graphqas",
      name: "graphqas-PNL",
      component: GraphQas,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "modulePenal"
      }
    },
    {
      path: "EditQuiz",
      name: "editQuizParam-PNL",
      component: EditQuizParam,
      meta: {
        premiumTag: "modulePenal"
      }
    },
    {
      path: "secteval",
      name: "secteval-PNL",
      component: SectEval,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "modulePenal"
      }
    },
    {
      path: "secteval-param",
      name: "secteval-param-PNL",
      component: SectEvalPenalOr,
      meta: {
        premiumTag: "modulePenal"
      }
    },

    {
      path: "evaluate/evaluaterisk",
      name: "evaluaterisk-PNL",
      component: EvaluateRisk,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "modulePenal"
      }
    },
    {
      path: "workplan",
      name: "workplan-PNL",
      component: WorkPlan,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "modulePenal"
      }
    },
    {
      path: "calendar",
      name: "calendar-PNL",
      component: Calendar,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "modulePenal"
      }
    },
    {
      path: "matrix",
      name: "matrix-PNL",
      component: RiskMatrix,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "modulePenal"
      }
    },
    {
      path: "secteval/quizRisk",
      name: "quizRiskParam-PNL",
      component: QuizRiskPenal,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "modulePenal"
      }
    },
    {
      path: "secteval/quizRisk",
      name: "quizRisk-PNL",
      component: QuizRisk,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "modulePenal"
      }
    },
    {
      path: "ganttChart",
      name: "ganttChart-PNL",
      component: GanttChart,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "modulePenal"
      }
    },
    {
      path: "actionplan",
      name: "actionplan-PNL",
      component: ActionPlan,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "modulePenal"
      }
    },
    {
      path: "auditoria",
      name: "auditoria-PNL",
      component: Auditoria,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "modulePenal"
      }
    }
  ]
};

let continuidadMenu = {
  path: "/continuidad",
  component: DashboardLayout,
  beforeEnter: verifySession,
  redirect: "/continuidad/identify/risk",
  children: [
    {
      path: "identify/risk",
      name: "risk-CNT",
      component: Risk,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleContinuidad"
      }
    },
    {
      path: "evaluate/riskMap",
      name: "riskMap-CNT",
      component: RiskMap,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleContinuidad"
      }
    },
    {
      path: "Monitoreo",
      name: "Monitoreo-CNT",
      component: Monitoreo,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleContinuidad"
      }
    },
    {
      path: "identify/ques",
      name: "ques-CNT",
      component: Ques,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleContinuidad"
      }
    },
    {
      path: "identify/graphqas",
      name: "graphqas-CNT",
      component: GraphQas,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleContinuidad"
      }
    },
    {
      path: "EditQuiz",
      name: "editQuizParam-CNT",
      component: EditQuizParam,
      meta: {
        premiumTag: "moduleContinuidad"
      }
    },
    {
      path: "secteval",
      name: "secteval-CNT",
      component: SectEval,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleContinuidad"
      }
    },
    {
      path: "secteval-param",
      name: "secteval-param-CNT",
      component: SectEvalPenalOr,
      meta: {
        premiumTag: "moduleContinuidad"
      }
    },

    {
      path: "evaluate/evaluaterisk",
      name: "evaluaterisk-CNT",
      component: EvaluateRisk,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleContinuidad"
      }
    },
    {
      path: "workplan",
      name: "workplan-CNT",
      component: WorkPlan,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleContinuidad"
      }
    },
    {
      path: "calendar",
      name: "calendar-CNT",
      component: Calendar,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleContinuidad"
      }
    },
    {
      path: "matrix",
      name: "matrix-CNT",
      component: RiskMatrix,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleContinuidad"
      }
    },
    {
      path: "secteval/quizRisk",
      name: "quizRiskParam-CNT",
      component: QuizRiskPenal,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleContinuidad"
      }
    },
    {
      path: "secteval/quizRisk",
      name: "quizRisk-CNT",
      component: QuizRisk,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleContinuidad"
      }
    },
    {
      path: "ganttChart",
      name: "ganttChart-CNT",
      component: GanttChart,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleContinuidad"
      }
    },
    {
      path: "actionplan",
      name: "actionplan-CNT",
      component: ActionPlan,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleContinuidad"
      }
    },
    {
      path: "auditoria",
      name: "auditoria-CNT",
      component: Auditoria,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleContinuidad"
      }
    }
  ]
};

let moneyMenu = {
  path: "/money",
  component: DashboardLayout,
  beforeEnter: verifySession,
  redirect: "/money/identify/risk",
  children: [
    {
      path: "identify/risk",
      name: "risk-MNY",
      component: Risk,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleMoney"
      }
    },
    {
      path: "evaluate/riskMap",
      name: "riskMap-MNY",
      component: RiskMap,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleMoney"
      }
    },
    {
      path: "Monitoreo",
      name: "Monitoreo-MNY",
      component: Monitoreo,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleMoney"
      }
    },
    {
      path: "identify/ques",
      name: "ques-MNY",
      component: Ques,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleMoney"
      }
    },
    {
      path: "identify/graphqas",
      name: "graphqas-MNY",
      component: GraphQas,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleMoney"
      }
    },
    {
      path: "EditQuiz",
      name: "editQuizParam-MNY",
      component: EditQuizParam,
      meta: {
        premiumTag: "moduleMoney"
      }
    },
    {
      path: "secteval",
      name: "secteval-MNY",
      component: SectEval,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleMoney"
      }
    },
    {
      path: "secteval-param",
      name: "secteval-param-MNY",
      component: SectEvalPenalOr,
      meta: {
        premiumTag: "moduleMoney"
      }
    },

    {
      path: "evaluate/evaluaterisk",
      name: "evaluaterisk-MNY",
      component: EvaluateRisk,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleMoney"
      }
    },
    {
      path: "workplan",
      name: "workplan-MNY",
      component: WorkPlan,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleMoney"
      }
    },
    {
      path: "calendar",
      name: "calendar-MNY",
      component: Calendar,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleMoney"
      }
    },
    {
      path: "matrix",
      name: "matrix-MNY",
      component: RiskMatrix,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleMoney"
      }
    },
    {
      path: "secteval/quizRisk",
      name: "quizRiskParam-MNY",
      component: QuizRiskPenal,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleMoney"
      }
    },
    {
      path: "secteval/quizRisk",
      name: "quizRisk-MNY",
      component: QuizRisk,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleMoney"
      }
    },
    {
      path: "ganttChart",
      name: "ganttChart-MNY",
      component: GanttChart,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleMoney"
      }
    },
    {
      path: "actionplan",
      name: "actionplan-MNY",
      component: ActionPlan,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleMoney"
      }
    },
    {
      path: "auditoria",
      name: "auditoria-MNY",
      component: Auditoria,
      beforeEnter: verifySession,
      meta: {
        premiumTag: "moduleMoney"
      }
    }
  ]
};

let loginPage = {
  path: "/login",
  name: "Login",
  component: Login
};

let registerPage = {
  path: "/register",
  name: "Register",
  component: Register
};

let recoverymailPage = {
  path: "/recoverymail",
  name: "Recoverymail",
  component: Recoverymail
};

let lockPage = {
  path: "/lock",
  name: "Lock",
  component: Lock
};
let accountPage = {
  path: "/account",
  name: "Account",
  component: Account
};
let passwordPage = {
  path: "/password",
  name: "Password",
  component: Password
};

// main routes

const routes = [
  {
    path: "/",
    name: "login",
    component: Login,
    redirect: "/login"
  },
  {
    path: "/password/:id",
    name: "password",
    component: Password,
    redirect: "/password"
  },
  {
    path: "/exit",
    name: "exit",
    component: Login,
    redirect: "/login"
    // beforeEnter(to, from, next) {
    //   window.location = "http://www.redcibercom.com.mx:8085/index.html"
    // }
  },
  {
    path: "/terminos",
    name: "terminos",
    beforeEnter(to, from, next) {
      window.location = URL_FRONT_CLIENT + "/terminos";
    }
  },
  {
    path: "/avisoPrivacidad",
    name: "privacy",
    beforeEnter(to, from, next) {
      window.location = URL_FRONT_CLIENT + "/avisoPrivacidad";
    }
  },

  userProfile,
  autoevaluationMenu,
  anticorruptionMenu,
  personaldataMenu,
  penalMenu,
  continuidadMenu,
  moneyMenu,
  padSystemMenu,
  padSystemAdmin,
  loginPage,
  registerPage,
  recoverymailPage,
  accountPage,
  passwordPage,
  lockPage,
  {
    path: "/admin/:id",
    name: "admin",
    component: DashboardLayout,
    redirect: "/admin/:id/overview",
    beforeEnter: verifySession,
    children: [
      {
        path: "/admin/:id/overview",
        name: "Overview",
        component: Overview,
        beforeEnter: verifySession
      },
      {
        path: "/admin/board",
        name: "AnticorruptionBoard",
        component: AnticorruptionBoard,
        beforeEnter: verifySession,
        meta: {
          premiumTag: "moduleAnticorruption"
        }
      }
    ]
  },
  {
    //Rutas de board de continuidad
    path: "/continuidad/:id",
    name: "adminContinuidad",
    component: DashboardLayout,
    redirect: "/continuidad/:id/overview",
    beforeEnter: verifySession,
    children: [
      {
        path: "/continuidad/:id/overview",
        name: "continuidadOverview",
        component: Overview,
        beforeEnter: verifySession
      },
      {
        path: "/continuidad/board",
        name: "continuidadBoard",
        component: AnticorruptionBoard,
        beforeEnter: verifySession,
        meta: {
          premiumTag: "moduleContinuidad"
        }
      }
    ]
  },
  {
    //Rutas de board de penal
    path: "/penal/:id",
    name: "adminPenal",
    component: DashboardLayout,
    redirect: "/penal/:id/overview",
    beforeEnter: verifySession,
    children: [
      {
        path: "/penal/:id/overview",
        name: "penalOverview",
        component: Overview,
        beforeEnter: verifySession
      },
      {
        path: "/penal/board",
        name: "penalBoard",
        component: AnticorruptionBoard,
        beforeEnter: verifySession,
        meta: {
          premiumTag: "modulePenal"
        }
      }
    ]
  },
  {
    //Rutas de board de Lavado de  Dinero
    path: "/money/:id",
    name: "moneyPenal",
    component: DashboardLayout,
    redirect: "/money/:id/overview",
    beforeEnter: verifySession,
    children: [
      {
        path: "/money/:id/overview",
        name: "moneyOverview",
        component: Overview,
        beforeEnter: verifySession
      },
      {
        path: "/money/board",
        name: "moneyBoard",
        component: AnticorruptionBoard,
        beforeEnter: verifySession,
        meta: {
          premiumTag: "moduleMoney"
        }
      }
    ]
  },
  {
    path: "/buying/:id",
    name: "buying",
    beforeEnter(to, from, next) {
      let paramsURL = "id=" + to.params.id;
      if (to.params.car !== undefined) {
        paramsURL += "&car=" + to.params.car;
      }
      window.location = URL_FRONT_CLIENT + "/buying?" + paramsURL;
    }
  },
  {
    path: "/pricing/:id",
    name: "pricing",
    beforeEnter(to, from, next) {
      window.location = URL_FRONT_CLIENT + "/buyPricing?id=" + to.params.id;
    }
  },
  { path: "*", component: NotFound }
];

export default routes;
