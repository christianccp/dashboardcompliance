import * as R from "ramda";
import { mapState, mapGetters, mapMutations } from "vuex";

export const gralMethods = {
  methods: {
    /**
     *
     * @param {String} propertyName
     * @param {Object} object
     */
    sortDesc(propertyName, object) {
      return R.sort(R.descend(R.prop(propertyName)), object);
    },
    /**
     *
     * @param {String} queryValue Valor a buscar
     * @param {Array} catalog
     * @return
     */
    getLabelLevel(queryValue, catalog) {
      let defaulLabel = "No definido";
      let label = defaulLabel;
      let property = "value";

      if (typeof catalog === "undefined") {
        return defaulLabel;
      }

      let catalogSorted = this.sortDesc(property, catalog);

      catalogSorted.forEach(item => {
        if (queryValue >= item.value && label === defaulLabel) {
          label = item.label;
        }
      });
      return label;
    },
    /**
     *
     * @param {String} queryValue Valor a buscar
     * @param {Array} catalog
     * @return
     */
    getLabelProbOrImp(queryValue, catalog) {
      let defaulLabel = "No definido";
      let label = defaulLabel;
      let property = "value";

      if (typeof catalog === "undefined") {
        return defaulLabel;
      }

      let catalogSorted = this.sortDesc(property, catalog);

      catalogSorted.forEach(item => {
        if (queryValue >= item.value && label === defaulLabel) {
          label = item.label;
        }
      });
      return label + "(" + queryValue + ")";
    },
    /**
     *
     * @param {String} queryValue
     * @param {Array} catalog
     */
    getLabel(queryValue, catalog) {
      let result = catalog.find(obj => obj.value == queryValue);
      return result ? result.label : "No definido";
    },
    /** Esta funcion permite generar de forma dinamica el 'name' del componente el cual sera invocado a traves
     * de vue-route. Al name resultante se le agrega un guion seguido por las iniciales del modulo.
     *  Ejemplo: Si moduleId es personaldata y el nameOrigin es 'risk', el nameResult sera 'risk-PD'
     * @param {String} nameOrigin name original del componente a llamar
     * @param {String} moduleId Id del modulo de donde se esta llamando la funcion.
     * @returns {String} name resultante despues de la evaluación.
     */
    _getNameRoute(nameOrigin, moduleId) {
      try {
        let nameResult = nameOrigin;
        switch (moduleId) {
          case "personaldata":
            nameResult = nameOrigin + "-PD";
            break;
          case "penal":
            nameResult = nameOrigin + "-PNL";
            break;
          case "money":
            nameResult = nameOrigin + "-MNY";
            break;
          case "continuidad":
            nameResult = nameOrigin + "-CNT";
            break;
          default:
            break;
        }
        //  console.log("_getNameRoute: ", nameResult);
        return nameResult;
      } catch (error) {
        console.error(error);
      }
    }
  }
};

export const gralErrorMethods = {
  methods: {
    /**
     *
     * @param {*} title
     * @param {*} message
     */
    showGenericError(title, message) {
      this.$notify({
        horizontalAlign: "center",
        message: message,
        timeout: 3000,
        title: "Error " + title,
        type: "danger"
      });
    },
    /**
     *
     */
    showNetworkError() {
      this.$notify({
        horizontalAlign: "center",
        timeout: 5000,
        message: "Error en la conexion",
        title: "Error",
        type: "danger"
      });
    },
    /**
     *
     */
    showTimeoutError() {
      this.$notify({
        horizontalAlign: "center",
        timeout: 5000,
        message:
          "El tiempo de espera ha sido superado. Revise su conexión de internet",
        title: "Sin conexión",
        type: "danger"
      });
    },
    /**
     *
     * @param {*} title
     * @param {*} message
     */
    showGenericSuccess(title, message) {
      this.$notify({
        horizontalAlign: "center",
        message: message,
        title: title,
        type: "success"
      });
    },
    /**
     *
     * @param {Object} error
     * @param {String} message Parametro opcional en caso de mostrar un mensaje personalizado
     */
    notifyError(error, message) {
      console.log("Entrando a notify error");
      if (error.code === "ECONNABORTED") {
        this.showTimeoutError();
        return;
      }
      if (error.message.trim() === "Network Error") {
        this.showNetworkError();
        return;
      }
      if ("response" in error) {
        this.showGenericError(
          "servidor",
          message + ": " + error.response.status
        );
      } else {
        this.showGenericError("Javascript", error);
        console.error(error);
      }
    }
  }
};

export const gralErrorLog = {
  methods: {
    /**
     *
     * @param {*} objMessage
     */
    writteGenericError(element, error, description) {
      let moduleId = this.$route.path.split("/")[1];
      let date = new Date().toISOString().split("T")[0];
      let dateHour = new Date();
      let hour = dateHour.toLocaleTimeString();
      let domain = localStorage.getItem("email")
        ? localStorage
            .getItem("email")
            .substring(
              localStorage.getItem("email").indexOf("@") + 1,
              localStorage.getItem("email").length
            )
        : "";
      let user = localStorage.getItem("email") || "";
      let page = window.location.pathname;
      this.$store.dispatch("systemlog/addSystemLog", {
        moduleId: moduleId,
        date: date,
        hour: hour,
        user: user,
        domain: domain,
        element: element,
        page: page,
        exceptionMessage: error.message,
        exceptionName: error.name,
        exceptionStack: error.stack,
        description: description
      });
    }
  }
};
