import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
import question from './modules/question'
import risk from './modules/risk'
import user from './modules/user'
import organization from './modules/organization'
import sections from './modules/sections'
import task from './modules/task'
import notifications from './modules/notifications'
import mail from './modules/mail'
import blog from './modules/blog'
import systemlog from './modules/systemlog'
import folders from './modules/folders'
import files from './modules/files'
import newsletters from './modules/newsletters'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    auth,
    user,
    question,
    risk,
    organization, 
    sections,
    task,
    notifications,
    mail,
    blog,
    systemlog,
    folders,
    files,
    newsletters
  },
  strict: debug,
  //plugins: debug ? [createLogger()] : []
})