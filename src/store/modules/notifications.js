import apiNotifications from 'src/api/notification/notification.js'
import {Notification} from 'src/constructors/notification.js'


const getLocalNotifications = () => {
    try {
      var notifications = JSON.parse(localStorage.getItem('localNotifications'))
      return notifications ? notifications : []
    } catch (error) {
      return []
    }   
}

const state = {
    notifications: getLocalNotifications(),
    typesN:{
        message: 'el-icon-chat-dot-square size-icon-two', 
        newTask: 'el-icon-document-add size-icon-three',
        checkTask:'el-icon-monitor size-icon-four', 
        audit:'el-icon-monitor size-icon-four', 
        sectionEnded:'el-icon-circle-check size-icon-one',
        revision: 'el-icon-copy-document size-icon-two'
    },
    loadingNotifications: false
}

const getters = {
}

const actions = {
    async setNotifications(context, paramsApi){
        if(context.state.loadingNotifications){
            return 
        }
        
        try{
            await context.commit('SET_LOADING_NOTIFICATION', true)
            await context.commit("RESET_NOTIFICATIONS")
            localStorage.setItem('localNotifications',JSON.stringify([]))
            let response = await apiNotifications.getNotifications(paramsApi)
            if(response.status === 200){
                response = response.data
                await response.map( notif => {
                    context.commit("SET_NOTIFICATION", new Notification(notif))
                })
            }
            await context.commit('SORT_NOTIFICATIONS')
            await context.commit('SET_LOADING_NOTIFICATION', false)
        }catch(error){
            await context.commit('SET_LOADING_NOTIFICATION', false)
            throw error
        }
    },

    async addNotification(context, paramsApi){
        try{
            let response = await apiNotifications.postNotification(paramsApi)
            await this._vm.$socket.emit("notificacion", paramsApi.orgId)
            return response
        }catch(error){
            throw error
        }
    },

    async deleteNotificationTarget(context, paramsApi){
        try{
            let response = await apiNotifications.deleteNotification(paramsApi)
            await context.dispatch("setNotifications", paramsApi)
            return response
        }catch(error){
            throw error
        }
    }


}

const mutations = {
    RESET_NOTIFICATIONS(state){
        state.notifications = []
    },
    SET_NOTIFICATION(state, notif){
        state.notifications.push(notif)
        localStorage.setItem('localNotifications',JSON.stringify(state.notifications))
    },
    SET_LOADING_NOTIFICATION(state, value){
        state.loadingNotifications = value
    },
    SORT_NOTIFICATIONS(state){
        state.notifications.sort(function (a,b){
            return new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime()
        })
    }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}