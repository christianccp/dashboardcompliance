import newslettersAPI from 'src/api/newsletters/newsletters.js'

const state = {
}

const getters = {
}

const actions = {
    async suscribeNewsletters(context, paramsAPI) {
        try {
            await newslettersAPI.suscribeNewsletters(paramsAPI)
        } catch (error) {
            throw error
        }
    }
}

const mutations = {
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}