import APISectInfo from 'src/api/organization/umbralSectInfo.js'
import Questions from 'src/api/question/question.js'
import apiQas from 'src/api/question/question.js'
import apiSection from 'src/api/question/section.js'
import {Question} from 'src/constructors/question.js'

const getLocalInfoSects = () => {
    try {
        var sections = JSON.parse(localStorage.getItem("InfoSections"))
        if(sections === null){
            return {sections:{},done:0, doing:0 }
        }else{
            return sections
        }
        
    } catch (error) {
        console.error(error)
        return {sections:{},done:0, doing:0 }
    }   
}


const state = {
    infoSections: getLocalInfoSects(),
    infoProgressSections: [],
    isEmpty: true,
    questions: [],
    finalTotal: 0,
    resultQuestions: []
}

const getters = {
    getSections(state){
        var see = state.infoSections.length > 0
        return {infoSections:state.infoSections, see: see}
    },
    NotAllSectionsAttended(state){
        if( state.infoProgressSections.length < 1){
            return false
        }
        let validSections = state.infoProgressSections.filter(section => section.questions.total > 0)
        let results = validSections.map( section => {
            return section.questions.total === section.questions.progress
        })
        let isThereIncompleteSection = results.filter(result => result === false)
        return isThereIncompleteSection.length > 0
    },
    sectionsWithQuestions(){
      return state.infoProgressSections.filter(card => card.questions.total > 0)        
    },
    sectionsWithQuestionsPenal(){
      return state.infoProgressSections.filter(card => true)        
    },
    sequentialSectionNumber(){
      let base = 0
      let sequential = 1
      state.infoProgressSections.map( section => {
        sequential = section.questions.typeOf.substr(7,section.questions.typeOf.length)
        sequential =  Number(sequential) + 1
      })
      return sequential;
    }
}

const actions = {
    async setSections(context, params){
        try{
            let resultsQuestions = []
            var totalDone = 0, totalDoing = 0
            let response = await APISectInfo.getSectInfo(params)
            response = response.data
            var sections = {}
            if(response.length > 0){
                sections = await response.map(async section=>{
                                    var done = 0, doing = 0
                                    var myParams = {access_token: params.access_token, moduleId: section.questions.moduleId, typeOf: section.questions.typeOf}
                                    var myQuestions = await Questions.getQuestions(myParams)
                                    myQuestions = myQuestions.data.rows
                                    resultsQuestions.push(myQuestions)
                                    if(myQuestions.length > 0){
                                        myQuestions.map(ques=>{
                                            if(ques.result === 1 || ques.result === 0.5){
                                                done += 1
                                            }else{
                                                doing += 1
                                            }
                                        })
                                        totalDone += done
                                        totalDoing += doing
                                    }      
                                    var obj = {title: section.questions.title, done: done, doing: doing}
                                    return obj
                                })
                let completed = await Promise.all(sections)
                const infoSects = {
                    sections: completed,
                    done:totalDone,
                    doing: totalDoing
                }
                context.commit('setResultSections', resultsQuestions)
                context.commit("updateSections", infoSects)
                
            }else{
                const infoSects = {
                    sections: {},
                    done:0,
                    doing: 0
                }
                
                context.commit("updateSections", infoSects)
            }
        }catch(error){
            throw error
        }
    },
    async getSectionsProgressInfo(context, params){
        try{
            let response = await APISectInfo.getSectInfo(params)
            response = response.data
            context.commit("SET_INFO_PROGRESS_SECTIONS", response)
        }catch(error){
            throw error
        }
    },
    async getSectionsAndQuestions(context, params){
        try{
            let response = await APISectInfo.getSectInfo(params)
            response = response.data
            var sections = []
            var completed = []
            if(response.length > 0){
                sections = await response.map(async section=>{
                                    var myParams = {access_token: params.access_token, moduleId: section.questions.moduleId, typeOf: section.questions.typeOf}
                                    var myQuestions = await Questions.getQuestions(myParams)
                                    myQuestions = myQuestions.data.rows 
                                    if(myQuestions.length >  0){
                                        var obj = {sections: section.questions.typeOf, title: section.questions.description, questions: myQuestions}
                                        return obj
                                    }else{
                                        return null
                                    }                                   
                                })
                completed = await Promise.all(sections)        
                completed = completed.filter(sect => sect !== null)        
            }
            return completed
        }catch(error){
            throw error
        }
    },
    async questions(context, paramsApi) {
        try {
            context.commit('RESET_QUESTIONS')

            let questions = await apiQas.getQuestions(paramsApi)

            // create a new object for each question retrive from back-end
            if( questions.data.count > 0){
            questions.data.rows.map((question)=>{    
                context.commit('SET_QUESTION', new Question(question))
            })
            context.commit('SET_EMPTY_STATE', false)
            }else{
            context.commit('SET_EMPTY_STATE', false)
            }          
            
        } catch (error) {
            throw error
        }
    },
    updateAns(context, param){
        context.commit('SET_NEW_VALUE', param)      
    },
    async saveProgress(context, param){      
        var results = []
        if(!context.state.isEmpty){
          context.state.questions.map(qas => {
            results.push({_id: qas.id, result:qas.result})
          })
          param.results = results
          return await apiQas.setResponses(param)
        }
    },
    async calculateTotalsAndSave(context, param){
        context.commit('RESET_FINAL_TOTAL')
        var results = []
        if(!context.state.isEmpty){
          context.state.questions.map(qas => {
            results.push({_id: qas.id, result:qas.result})
            context.commit('ADD_VALUE_TO_FINAL_TOTAL', (qas.result * qas.weight))
          })
          param.results = results
          return await apiQas.setResponses(param)
        }
    },
    async calculateValuesParamsMatrix(context, param){
      try{
        var results = []
        if(!context.state.isEmpty){
          let typeOfGral = param.typeOf.substring(0, param.typeOf.length - 3)
          let finalType = param.typeOf.substring(param.typeOf.length - 3)
          let matrixParamsValue = [0, 0, 0, 0]
          let levels = context.rootGetters['organization/levels']
          levels = JSON.stringify(levels)
          levels = JSON.parse(levels)
          levels.sort(function(a, b){
            return a.value-b.value
          })
          let subrisks = context.rootState.risk.subriskTable
          subrisks = JSON.stringify(subrisks)
          subrisks = JSON.parse(subrisks)
          //Actualizar respuestas en la BD
          context.state.questions.map(qas => {
            results.push({_id: qas.id, result:qas.result})
          })
          param.results = results
          await apiQas.setResponses(param)

          //Hacer calculo de valores de parametros en matrix
          let newFinalType = finalType === 'Nac' ? 'Int' : 'Nac'
          let paramsApiQas = {
            access_token: param.access_token, 
            moduleId: param.moduleId,
            typeOf: typeOfGral + newFinalType 
          }
          let questions = await apiQas.getQuestions(paramsApiQas)
          questions = questions.data.rows
          switch(typeOfGral){
            case 'subfactorProb':

              //Suma primer cuestionario
              context.state.questions.map(qas => {
                if(qas.result === 0 || qas.result === 1){
                  matrixParamsValue[0] += qas.result
                }
              })

              //Suma segundo cuestionario
              questions.map(qas => {
                if(qas.result === 0 || qas.result === 1){
                  matrixParamsValue[0] += qas.result
                }
              })

              subrisks.map(async subrisk => {
                let sumaImp = 0
                let sumaProb = 0
                subrisk.params.map(parameter => {
                  parameter.probability.value = matrixParamsValue[0]
                  sumaImp += parameter.impact.value
                  sumaProb += parameter.probability.value
                })
                sumaImp = Math.round(sumaImp/subrisk.params.length)
                sumaProb = Math.round(sumaProb/subrisk.params.length)
                sumaImp = !isNaN(sumaImp) ? sumaImp: 0
                sumaProb = !isNaN(sumaProb) ? sumaProb: 0 
                let myLevel = sumaImp * sumaProb
                let label = 'No asignado'
                levels.forEach((item) => {
                  if (myLevel >= item.value &&  label === 'No asignado' ) {
                    label = item.label
                  }
                })
                subrisk.params.map(async myParam =>{
                  myParam.level.value = myParam.impact.value * myParam.probability.value
                  let paramsMatrix = {
                    access_token: param.access_token,
                    subriskId: subrisk._id,
                    levelInhAvg:{value: myLevel, label: label },
                    probabilityAvg: {value: sumaProb}, 
                    impactAvg:{value: sumaImp},	
                    params: myParam
                  }
                  await context.dispatch('risk/updateSubriskParams', paramsMatrix, { root: true })
                })
              })
            break;

            case 'subfactorImp':
              //Suma del primer cuestionario
              context.state.questions.map(qas => {
                switch(qas.questionNumber){
                  case 2:
                    if(qas.result === 0 || qas.result === 1){
                      matrixParamsValue[0] += qas.result
                      matrixParamsValue[1] += qas.result
                      matrixParamsValue[2] += qas.result
                    }
                  break;
                  case 7:
                    if(qas.result === 0 || qas.result === 1){
                      matrixParamsValue[0] += qas.result
                      matrixParamsValue[1] += qas.result
                    }
                  break;
                  case 9:
                    if(qas.result === 0 || qas.result === 1){
                      matrixParamsValue[0] += qas.result
                      matrixParamsValue[1] += qas.result
                      matrixParamsValue[3] += qas.result
                    }
                  break;
                  default:
                    if(qas.result === 0 || qas.result === 1){
                      matrixParamsValue[0] += qas.result
                      matrixParamsValue[1] += qas.result
                      matrixParamsValue[2] += qas.result
                      matrixParamsValue[3] += qas.result
                    }
                  break;
                }
              })

              //Suma del segundo cuestionario
              questions.map(qas => {
                switch(qas.questionNumber){
                  case 2:
                    if(qas.result === 0 || qas.result === 1){
                      matrixParamsValue[0] += qas.result
                      matrixParamsValue[1] += qas.result
                      matrixParamsValue[2] += qas.result
                    }
                  break;
                  case 7:
                    if(qas.result === 0 || qas.result === 1){
                      matrixParamsValue[0] += qas.result
                      matrixParamsValue[1] += qas.result
                    }
                  break;
                  case 9:
                    if(qas.result === 0 || qas.result === 1){
                      matrixParamsValue[0] += qas.result
                      matrixParamsValue[1] += qas.result
                      matrixParamsValue[3] += qas.result
                    }
                  break;
                  default:
                    if(qas.result === 0 || qas.result === 1){
                      matrixParamsValue[0] += qas.result
                      matrixParamsValue[1] += qas.result
                      matrixParamsValue[2] += qas.result
                      matrixParamsValue[3] += qas.result
                    }
                  break;
                }
              })
              subrisks.map(async subrisk => {
                let sumaImp = 0
                let sumaProb = 0
                subrisk.params.map(parameter => {
                  switch(parameter._id){
                    case '5de6b774a6ed750017fa37da':
                      parameter.impact.value = matrixParamsValue[0]
                    break;
                    case '5de6b998a6ed750017fa37db':
                      parameter.impact.value = matrixParamsValue[1]
                    break;
                    case '5de6ba2ba6ed750017fa37dc':
                      parameter.impact.value = matrixParamsValue[2]
                    break;
                    case '5de6bc65a6ed750017fa37dd':
                      parameter.impact.value = matrixParamsValue[3]
                    break;
                  }
                  sumaImp += parameter.impact.value
                  sumaProb += parameter.probability.value
                })
                sumaImp = Math.round(sumaImp/subrisk.params.length)
                sumaProb = Math.round(sumaProb/subrisk.params.length)
                sumaImp = !isNaN(sumaImp) ? sumaImp: 0
                sumaProb = !isNaN(sumaProb) ? sumaProb: 0 
                let myLevel = sumaImp * sumaProb
                let label = 'No asignado'
                levels.forEach((item) => {
                  if (myLevel >= item.value &&  label === 'No asignado' ) {
                    label = item.label
                  }
                })
                subrisk.params.map(async myParam =>{
                  myParam.level.value = myParam.impact.value * myParam.probability.value
                  let paramsMatrix = {
                    access_token: param.access_token,
                    subriskId: subrisk._id,
                    levelInhAvg:{value: myLevel, label: label },
                    probabilityAvg: {value: sumaProb}, 
                    impactAvg:{value: sumaImp},	
                    params: myParam
                  }
                  await context.dispatch('risk/updateSubriskParams', paramsMatrix, { root: true })
                })
              })
            break;
          }

        }
      }catch(error){
        throw error
      }
    },
    async newSection (context, paramsApi) {
      let res = await apiSection.newSection(paramsApi)
      let { moduleId, description, typeOf, title } = paramsApi
      let infoProgressObject = {
        _id: res.data.id,
        questions: {
          moduleId,
          description,
          typeOf,
          title,
          progress: 0,
          total: paramsApi.questions.length,
          _id: res.data.id
        }
      }
      context.commit('ADD_INFO_PROGRESS_SECTIONS', infoProgressObject)
      return res
    },
    async deleteSection (context, paramsApi) {
      let res = await apiSection.deleteSection(paramsApi)
      if (res.status == 200){
        context.commit('DELETE_INFO_PROGRESS_SECTIONS', paramsApi.id)
      }
      return res
    },
    async updateSection (context, paramsApi) {
      let res = await apiSection.updateSection(paramsApi)
      context.commit('UPDATE_INFO_PROGRESS_SECTIONS', paramsApi)
      return res 
    },
    async updateInfoProgressSections(context, paramsApi) {
      let res = await APISectInfo.updateInfoOrg(paramsApi)
      context.commit('UPDATE_INFO_PROGRESS_SECTIONS', paramsApi)
      return res 
    }
}

const mutations = {
    setResultSections(state, data) {
        state.resultQuestions = data
    },
    updateSections(state, newSections){
      state.infoSections = newSections
      localStorage.setItem("InfoSections", JSON.stringify(newSections))
    },
    ADD_INFO_PROGRESS_SECTIONS(state, paramsApi){
      state.infoProgressSections.push(paramsApi)
    },
    UPDATE_INFO_PROGRESS_SECTIONS(state, paramsApi){
      // si entra en este if, significa que esta actualizando las preguntas de la sección
      if (paramsApi.questionsToDelete) {
        let {idSection} = paramsApi
        let index = state.infoProgressSections.findIndex(section => {
          return section.questions._id === idSection
        }) 
        state.infoProgressSections[index].questions.total -= paramsApi.questionsToDelete.length
        state.infoProgressSections[index].questions.total += paramsApi.questionsToAdd.length
        state.infoProgressSections[index].questions.progress = 0
        return 
      }
      // si entra en este if, significa que esta actualizando el nombre de la sección
      if (paramsApi.questions || paramsApi.questions.description){
        let {id} = paramsApi
        let index = state.infoProgressSections.findIndex(section => {
          return section.questions._id === id
        }) 
        state.infoProgressSections[index].questions.description = paramsApi.questions.description
      }
    },
    DELETE_INFO_PROGRESS_SECTIONS(state, id){
      state.infoProgressSections = state.infoProgressSections.filter(section => {
        if (section.questions._id) {
          return section.questions._id === id ? false : true 
        } else {
          return true
        }
      })
    },
    SET_INFO_PROGRESS_SECTIONS(state, infoProgress){
      state.infoProgressSections = infoProgress
    },
    ADD_VALUE_TO_FINAL_TOTAL(state, value){
        state.finalTotal += value
    },
    RESET_QUESTIONS(state){
        state.questions = []
    },
    RESET_FINAL_TOTAL(state){
        state.finalTotal = 0
    },
    SET_QUESTION(state, question){
        state.questions.push(question)
    },
    SET_EMPTY_STATE(state, value){
        state.isEmpty = value
    },
    SET_NEW_VALUE(state, param){
        state.questions[param.index].result = param.value
    }    
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}