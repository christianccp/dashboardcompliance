import systemLog from 'src/api/systemLog/systemLog.js'
import * as functions from 'src/plugins/generalFunctions.js'
import * as ganttFunctions from 'src/plugins/ganttFunctions.js'
import * as R from 'ramda'
import Vue from 'vue'

const state = {
}

const getters = {
}

const actions = {
  async getSystemLog(context, paramsApi) { 
    try {
      let response = await systemLog.getLog(paramsApi)
      return response
    } catch (error) {
      throw error
    }
  },  
  async addSystemLog(context, paramsApi) { 
    try {
      let response = await systemLog.addLog(paramsApi)
      return response
    } catch (error) {
      throw error
    }
  }, 
}

const mutations = {
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}