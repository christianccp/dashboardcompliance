import blogs from 'src/api/blogs/blogs.js'

const state = {
    blogsTable:[]

}

const getters = {
  // TODO: leer no solo el registro 0 
 blogsTable(state) {
   return (state.blogsTable) ? state.blogsTable : null
 }
}

const actions = {
    async selectBlogs(context, type) {
        try {
          let arrayblogs = await blogs.getBlogs(type)
          context.commit('SET_BLOGS_TABLE', arrayblogs.data)
        } catch (error) {
          context.commit('RESET_BLOGS')
          throw error
        }
    },
    async updateBlog(context, paramsApi) {
        try {
          let response = await blogs.updateBlog(paramsApi)
          context.commit('UPDATE_BLOG', response.data)
          return response.status
        } catch (error) {
          throw error
        }
      },
      async addBlog(context, paramsApi) { 
        try {
          let response = await blogs.insertBlog(paramsApi)
          context.commit('ADD_BLOG', response.data)
          return response.status
        } catch (error) {
          throw error
        }
      },
      async deleteBlog(context, paramsApi) { 
        try {
          let response = await blogs.deleteBlog(paramsApi)
          context.commit('DELETE_SUBRISK', paramsApi.id)
          return response.status
        } catch (error) {
          throw error
        }
      }, 
}

const mutations = {
    SET_BLOGS_TABLE(state, blogsTable) {
        state.blogsTable = blogsTable
    },
    UPDATE_BLOG(state, data){
        let indexFound = state.blogsTable.findIndex(element => element.id === data.id)
        state.blogsTable[indexFound] = data
    },
    ADD_BLOG(state, data){
        state.blogsTable.push(data)
    },
    DELETE_SUBRISK(state, idToDelete){
        let indexFound = state.blogsTable.findIndex(element => element.id === idToDelete)
        state.blogsTable.splice(indexFound,1)
    },
    RESET_BLOGS (state) {
      state.blogsTable = []
  },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}