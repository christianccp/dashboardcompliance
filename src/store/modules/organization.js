import umbralsApi from "src/api/organization/umbralParams";
import infoOrganization from "src/api/organization/organization";
import * as R from "ramda";
import store from "..";

const getLocalParams = () => {
  try {
    return JSON.parse(localStorage.getItem("paramsUmbral"));
  } catch (error) {
    console.error(error);
    return null;
  }
};

const getCatalogAreas = () => {
  return [
    { value: "legal", name: "Área Legal", authorizer: null },
    { value: "HR", name: "Área Recursos Humanos" },
    { value: "finance", name: "Área Finanzas" },
    { value: "fiscal", name: "Área Fiscal" },
    { value: "comercial", name: "Área Comercial" },
    { value: "compliance", name: "Área Compliance" },
    {
      value: "IT",
      name: "Área Seguridad de la Información y Datos Personales"
    },
    { value: "accounting", name: "Área Contable" }
  ];
};

const getResidualCatalog = () => {
  return [
    { value: 0, valueMax: 33.1, label: "Alto", color: "#FF0040" },
    { value: 33.1, valueMax: 66, label: "Medio", color: "#FABE3C" },
    { value: 66, valueMax: 100, label: "Bajo", color: "#35A97A" }
  ];
};

const state = {
  name: "",
  domain: "",
  url: "",
  zipcode: "",
  city: "",
  country: "",
  areas: [],
  paramsUmbral: getLocalParams(),
  residualCatalog: getResidualCatalog(),
  premium: {},
  roles: {
    admin: "Administrador",
    user: "Usuario"
  },
  modules: ["anticorruption", "money", "penal", "continuidad"]
};

const getters = {
  // TODO: leer no solo el registro 0
  impacts(state) {
    return state.paramsUmbral.impacts ? state.paramsUmbral.impacts : null;
  },
  probabilities(state) {
    return state.paramsUmbral.probabilities
      ? state.paramsUmbral.probabilities
      : null;
  },
  // TODO: sort asc levels
  levels(state) {
    return state.paramsUmbral.levels ? state.paramsUmbral.levels : null;
  },
  authByArea(state) {
    let authorizersByArea = {};

    state.areas.map(area => {
      authorizersByArea[area.value] = area.authorizer ? area.authorizer : null;
    });

    return authorizersByArea;
    //return state.areas.map(area => area.value)
  }
};

const actions = {
  async getInfoOrg(context, paramsApi) {
    try {
      var res = await infoOrganization.getInfoOrg(paramsApi);
      await context.commit("SET_INFO_ORG", res.data);
    } catch (error) {
      throw error;
    }
  },
  async getAreasOrg(context, paramsApi) {
    try {
      let res = await infoOrganization.getInfoOrg(paramsApi);
      await context.commit("SET_AREAS", res.data.areas);
    } catch (error) {
      throw error;
    }
  },
  async updateInfoOrg(context, paramsApi) {
    try {
      var { access_token, org_id } = paramsApi;
      var org = { access_token, org_id };
      org.name = context.state.name;
      org.url = context.state.url;
      org.zipcode = context.state.zipcode;
      org.city = context.state.city;
      org.country = context.state.country;
      var res = await infoOrganization.putInfoProfileOrg(org);
    } catch (error) {
      throw error;
    }
  },
  selectUmbrals(context, paramsApi) {
    return umbralsApi.getUmbralParams(paramsApi).then(response => {
      const paramsUmbral = response.data;
      context.commit("setUmbrales", paramsUmbral ? paramsUmbral : {});
    });
  },
  updateUmbrals(context, paramsApi) {
    return umbralsApi.setUmbralParams(paramsApi).then(response => {
      let umbrals = response.data.params.find(
        param => param.moduleId === paramsApi.params.moduleId
      );
      context.commit("setUmbrales", umbrals ? umbrals : {});
    });
  },
  updateAreaAuthorizer(context, paramsApi) {
    context.commit("SET_AREA_AUTH", paramsApi);
    return infoOrganization.updateAreaAuthorizer(paramsApi);
  }
};

const mutations = {
  setUmbrales(state, paramsUmbral) {
    state.paramsUmbral = paramsUmbral;
    localStorage.setItem("paramsUmbral", JSON.stringify(state.paramsUmbral));
  },
  SET_INFO_ORG(state, data) {
    state.domain =
      data.domain != null && data.domain != undefined ? data.domain : "";
    state.name = data.name != null && data.name != undefined ? data.name : "";
    state.url = data.url != null && data.url != undefined ? data.url : "";
    state.zipcode =
      data.zipcode != null && data.zipcode != undefined ? data.zipcode : "";
    state.city = data.city != null && data.city != undefined ? data.city : "";
    state.country =
      data.country != null && data.country != undefined ? data.country : "";
    //state.country = (data.country != null && data.country != undefined) ? data.country : ''
    state.premium =
      data.premium != null &&
      data.premium != undefined &&
      R.is(Object, data.premium)
        ? data.premium
        : {};
    if (state.premium) {
      //Setea vigenciaContinuidad y las demas vigencias en true siempre, hasta tener campo de vigencia
      let fechaInicio = state.premium.vigContinuidad;
      fechaInicio = fechaInicio.substring(0, 10);
      //console.log("F INIC", fechaInicio);
      var fechaVig = new Date(fechaInicio).getTime();
      //----------Obtener fecha de hoy
      var today = new Date();
      var diff = fechaVig - today;
      //console.log("Today" + today.getTime());
      diff = diff / (1000 * 60 * 60 * 24);
      if (diff > 0) {
        state.premium.vigenciaContinuidad = true;
        state.premium.vigenciaAnticorruption = true;
        state.premium.vigenciaPenal = true;
        state.premium.vigenciaMoney = true;
      } else {
        state.premium.vigenciaContinuidad = false;
        state.premium.vigenciaAnticorruption = true;
        state.premium.vigenciaPenal = true;
        state.premium.vigenciaMoney = true;
      }
      //console.log("Premium" + JSON.stringify(state.premium));
    }
    if (Object.values(state.premium).find(product => product)) {
      state.premium["serviceUserProfile"] = true;
    } else {
      state.premium["serviceUserProfile"] = false;
    }
    let modulesValues = Object.keys(state.premium)
      .filter(key => key.includes("module"))
      .reduce((obj, key) => {
        obj[key] = state.premium[key];
        return obj;
      }, {});
    if (Object.values(modulesValues).find(product => product)) {
      state.premium["someModuleObtained"] = true;
    } else {
      state.premium["someModuleObtained"] = false;
    }
  },
  SET_URL(state, url) {
    state.url = url;
  },
  SET_NAME(state, name) {
    state.name = name;
  },
  SET_ZIPCODE(state, zipcode) {
    state.zipcode = zipcode;
  },
  SET_CITY(state, city) {
    state.city = city;
  },
  SET_COUNTRY(state, country) {
    state.country = country;
  },
  SET_AREAS(state, areas) {
    state.areas = areas;
  },
  SET_AREA_AUTH(state, data) {
    let { authorizer, value } = data;
    state.areas.find(area => {
      if (area.value == value) {
        area.authorizer = authorizer;
      }
    });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
