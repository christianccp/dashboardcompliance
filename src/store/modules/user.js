import userAPI from 'src/api/user/user'
import mail from 'src/api/mail/mail'
import {User} from 'src/constructors/user.js'
import * as R from 'ramda'
import Vue from 'vue'

const state = {
  myUser: new User(),
  members: [],
  membersFiltered: [],
  registerInfo: {
    name: '',
    email: '',
    mobile: '',
    phone: '',
    pais: '',
    ocupation: '',
    isAuditor:'',
    organization: {
      numEmployments:0,
      url:'',
      zipcode:'',
      module: '',
      domain: ''
    },
    paramsURL: ''
  }
}

const getters ={
  dashboardFlags(state){
    return state.myUser.flags.dashboard
  },
  candidatesAdmin (state) {
    return state.members.filter(member => member.roleDocMgt != 'owner' ? member : null)
  },
  candidatesLead(state) {
    return state.membersFiltered.filter(member => member.roleDocMgt != 'owner' && member.roleDocMgt != 'admin'? member : null)
  }
}

const actions={
  async updateProfilePhoto(context, params){
    try{
      params._id = context.state.myUser._id
      var response = await userAPI.changeProfilePhoto(params)
      response = response.data
      await context.commit("setimagen", response.uploadedFiles[0].location)
      let data = {
        _id: context.state.myUser._id,
        picture: context.state.myUser.picture
      }
      await context.commit("UPDATE_MEMBER_INFO", data)
    }catch(error){
      throw error
    }
  },
  async getUser(context, params){    
    try{
      var res = await userAPI.getUsuario()
      await context.commit("SET_USER", res.data)
    }catch(error){
      throw error
    }
  },
  async updateUserInfo(context, userInfo){
    try{
      let res = await userAPI.updateUsuario(userInfo.id, userInfo)
      if (res.data.id == context.state.myUser._id) {
        await context.commit("SET_USER", res.data, userInfo.access_token)
        let data = {
          _id: context.state.myUser._id,
          name: context.state.myUser.name,
          role: context.state.myUser.role,
          picture: context.state.myUser.picture,
          roleDocMgt: context.state.myUser.roleDocMgt,
          ocupation: context.state.myUser.ocupation,
          isAuditor: context.state.myUser.isAuditor
        }
        await context.commit("UPDATE_MEMBER_INFO", data)
      } else {
        let data = {
          _id: res.data.id,
          name: res.data.name,
          role: res.data.role,
          picture: res.data.picture,
          roleDocMgt: res.data.roleDocMgt,
          ocupation: res.data.ocupation,
          isAuditor: res.data.isAuditor
        }
        await context.commit("UPDATE_MEMBER_INFO", data)
      }
      return res
    }catch(error){
      throw error
    }
  },
  async getUserExist(context, params){
    try{
      let res = await userAPI.getUserExist(params)
      if (res.data.id) {
        return res
      }
      return res
    }catch(error){
      throw error
    }
  },
  async clearPassword(context, params){
    try{
      let res = await userAPI.clearPassword(params)
      if (res.data.id) {
        return res
      }
      return res
    }catch(error){
      console.log("Error"+error)
      throw error
    }
  },
  async getMembers (context, token) {
    try {
      var users = await userAPI.getOrgMembers(token)
      var orgMembers = users.data.users
      //orgMembers = orgMembers.filter(user => user._id != users.data.idOwner)
      await context.commit("SET_MEMBERS", orgMembers)
    } catch (error) {
      throw error
    }
  },
  async deleteMember (context, param) {
    try {
      await userAPI.deleteUser(param)
      await context.commit("DELETE_MEMBER", param.id)
    } catch (error) {
      throw error
    }
  },
  async updateMemberRole (context, data) {
    try {
      var { index, value, access_token } = data
      await userAPI.assignRole({ id: context.state.members[index]._id, role: value, access_token })
      await context.commit("SET_MEMBER_ROLE", { index, value })
    } catch (error) {
      throw error
    }
  },
  updateMemberDocMgtRole (context, userInfo) {
    context.commit("SET_MEMBER_DOCMGT_ROLE", userInfo)
    return new Promise((resolve, reject) => {
      context.dispatch("updateUserInfo", userInfo)
      .then(response => { 
          resolve(response);
      })
      .catch(error => {
        reject(error)
      })
    })
  },
  

  selectimagen(context,file) { 
    context.commit('setimagen', file)
  },
  select_name(context,file) { 
    context.commit('set_name', file)
  },
  select_phone(context,file) { 
    context.commit('set_phone', file)
  },
  select_mobile(context,file) { 
    context.commit('set_mobile', file)
  },
  select_status_modal_upload(context,file) { 
    context.commit('set_status_modal_upload', file)
  },
  select_access_token(context,file) { 
    context.commit('set_access_token', file)
  },
  select_id(context,file) { 
    context.commit('set_id', file)
  },
  async setFlags(context,params){
    try{
      context.commit('SET_FLAGS',params.flags)
      await userAPI.setFlagsUser(params)
    }catch(e){
      throw e
    }
  }
}
const mutations= {
  SET_USER(state, user, token){
    state.myUser = new User(user)
  },
  setimagen(state, picture) {
    state.myUser.picture = picture
  },
  set_name(state, name) {
    state.myUser.name = name
  },
  set_phone(state, phone) {
    state.myUser.phone= phone
  },
  set_mobile(state, mobile) {
    state.myUser.mobile = mobile
  },
  set_status_modal_upload(state, status) {
    state.status_modal_upload = status
  },
  set_access_token(state,token){
    state.access_token=token
  },
  set_id(state,id){
    state.myUser._id=id
  },
  SET_OCUPATION(state, ocupation){
    state.myUser.ocupation = ocupation
  },
  SET_FLAGS(state, flags){
    state.myUser.flags = R.mergeDeepRight(state.myUser.flags, flags)
  },
  SET_ROLEDOCMGT(state, roleDocMgt){
    state.myUser.roleDocMgt = roleDocMgt
  },
  SET_MEMBERS (state, members) {
    state.members = members
  },
  ADD_MEMBER (state, member){
    state.members.push(member)
  },
  DELETE_MEMBER (state, id) {
    state.members = state.members.filter(member => member._id != id)
  },
  SET_MEMBER_ROLE (state, data) {
    var { index, value } = data
    state.members[index].role = value
    if(state.members[index]._id == state.myUser._id){
      state.myUser.role = value
    }
  },
  SET_MEMBER_DOCMGT_ROLE (state, data) {
    let { id, roleDocMgt } = data
    state.members.find(member => {
      if (member._id == id) {
        member.roleDocMgt = roleDocMgt
      }
    })
  },
  SET_MEMBERS_FILTERED (state, filter) {
    if (filter) {
      state.membersFiltered = state.members.filter(member => member.ocupation === filter ? member : null)
    } else {
      state.membersFiltered = state.members
    }
  },
  UPDATE_MEMBER_INFO(state, info){
    state.members.map( member =>{
      if(member._id == info._id){
        member = Object.assign(member, info)
      }
    })
    if(state.members.length > 0){
      Vue.set(state.members, 0, state.members[0])
    }
    if( info._id == state.myUser._id){
      state.myUser = Object.assign(state.myUser, info)
    }
  },
  SET_DATA_REGISTER(state, data){
    state.registerInfo.name = data.name
    state.registerInfo.email = data.email
    state.registerInfo.phone = data.phone
    state.registerInfo.mobile = data.mobile
    state.registerInfo.pais = data.pais
    state.registerInfo.paramsURL = data.paramsURL
    state.registerInfo.organization.numEmployments = data.organization.numEmployments
    state.registerInfo.organization.income = data.organization.income
    state.registerInfo.organization.module = data.organization.module
    state.registerInfo.ocupation = data.ocupation
    state.registerInfo.organization.url = data.organization.url
    state.registerInfo.organization.zipcode = data.organization.zipcode
    state.registerInfo.isAuditor = data.isAuditor
  }
}
  
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}