import sendMails from 'src/api/mail/mail.js'

const state = {

}

const getters = {

}

const actions = {
    async mails(context, paramsApi) {
        try {
            let response = await sendMails.sendMail(paramsApi)
            console.log(response.data)
            return response.status
        } catch (error) {
            throw error
        }
      },
      async blockUser(context, paramsApi) {
        try {
            let response = await sendMails.blockUser(paramsApi)
            return response.status
        } catch (error) {
            throw error
        }
      }
}

const mutations = {

}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}