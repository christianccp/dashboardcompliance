import apiQas from 'src/api/question/question.js'
import apiSection from 'src/api/question/section.js'
import {Question, Result} from 'src/constructors/question.js'
import * as functions from 'src/plugins/generalFunctions.js'

const TYPE_OF = 'questionnaire1'

const state = {
  isEmpty: true,
  showDrawer: false,
  questions: [],
  //  Object Structure   'dataByTab'  
  //    _id: document id of question collection 
  //    weight: question's weight 
  //    results[].subrisk_id: document id of subrisk collection 
  //    results[].subriskTitle: subrisk's title
  //    results[].value: value of radio button
  //  Example --> [{"_id":"123...","weight": 10,"results":[{"subrisk_id":"1234...","subriskTitle":"soborno","value":1}]},{...}] 
  dataByTab: [],
  //  Object Structure   'residualBySubrisk'  
  //    subrisk_id: document id of subrisk collection to know which document is gonna be updated
  //    levelResidualAvg.label: subrisk's residual label  
  //    levelResidualAvg.value: subrisk's residual value 
  //  Example --> [{"subrisk_id":"5cb4034f86c22e4c15df27cc","levelResidualAvg":{"label":"No definido","value":1}},{...}]
  residualBySubrisk: []
}

const getters= {

  // get results of a question by question's id. To call this getter use following sentence in ur component:
  // this.$store.getters['question/getResultsById']("73838..")
  getResultsById (state) {
    return id => {
      let question = state.questions.find(questions => questions.id === id)
      return question.results
    }
  },
  
  // get results of a question by question's index. 
  getResultsByIndex (state) {
    return index => state.questions[index].results
  }
}

const actions = {
  async questions(context, paramsApi) {
    try {
      paramsApi.typeOf = TYPE_OF
      context.commit('RESET_QUESTIONS')
      context.commit('RESET_DATA_BY_TAB')
      context.commit('RESET_RESIDUAL_SUBRISK')

      let questions = await apiQas.getQuestions(paramsApi)

      // create a new object for each question retrive from back-end
      questions.data.rows.map((question)=>{

        // context.commit('SET_QUESTION', new Question(question))
        
        // pass 'results' to a new object to handle state until user save explicitly using button guardar/continuar despues
        let resultsAux = []
        let results = []
        resultsAux = question.results.map(result =>  new Result(result))
        
        // if 'results' are empty at this point means user has not asnwer the questionnaire before, so we initialize 'result' depending on
        // subrisks information
        var risks = context.rootState.risk.subriskTable
        if (resultsAux.length > 0) {     
          risks.forEach(risk => {
            let aux = resultsAux.find(sub => sub.subrisk_id == risk._id)
            let result = aux? new Result(aux) : new Result()
            if(aux === undefined || aux === null){
              result.subrisk_id = risk._id
              result.subriskTitle = risk.title
            }            
            results.push(result)
          })
        }else{
          risks.forEach(risk => {
            let result = new Result()      
            result.subrisk_id = risk._id
            result.subriskTitle = risk.title
            results.push(result)
          })
        }
        //console.log("Final Results", results)
        question.results = results
        context.commit('SET_QUESTION', new Question(question))
        // set serie of data for each question and adding their '_id' document and weight linked.
        context.commit('SET_DATA_BY_TAB', { _id: question.id, weight: question.weight, results})
      })
    } catch (error) {
      throw error
    }
  },
  async updateQuestions (context, paramsApi) {
    let res = await apiSection.updateSection(paramsApi)
    return res 
  },
  addResultInDataByTab(context, newSubriskResultData){
    context.commit("ADD_NEW_SUBRISK_RESULT", newSubriskResultData)
  },
  deleteResultInDataByTab(context, id){
    context.commit("DELETE_NEW_SUBRISK_RESULT", id)
  },
  async saveProgress(context, paramsApi) {
    try {
      paramsApi.results = context.state.dataByTab 
      paramsApi.typeOf = TYPE_OF
      return await apiQas.setResponses(paramsApi)
    } catch (error) {
      throw error
    }
  },
  async calculateTotalsAndSave(context, paramsApi) {
    try {
      // calculate totals by subrisk
      context.state.dataByTab.map( data => {
        data.results.map ( result => {

          // calculate total multiplying weight of question by radio button value
          let total = data.weight * result.value

          // check if total has a valid value
          if (total < 0  || typeof total === undefined || total === null ) {
            return 
          }

          // get residual catalog from another organization vuex module
          let residualCatalog = context.rootState.organization.residualCatalog

          // find if subrisk has already been added to residualBySubrisk
          let index = context.state.residualBySubrisk.findIndex(element => element.subrisk_id === result.subrisk_id )

          // if subrisk not found, add as a new element into residualBySubrisk array, otherwise add the total to the subrisk existing 
          if (index === -1){
            // get label of residual value
            let labelResidual = functions.getLabelLevel(total,residualCatalog)
            // add residual level to residualBySubrisk
            context.commit('ADD_ELEMENT_RESIDUAL_SUBRISK',{subrisk_id: result.subrisk_id, levelResidualAvg: { label: labelResidual, value: total } })
          } else {
            // increment value of residual level 
            context.commit('INC_RESIDUAL_SUBRISK',{index, total })
            
            let residualValue = context.state.residualBySubrisk[index].levelResidualAvg.value
            // get label of residual level value
            let label = functions.getLabelLevel(residualValue,residualCatalog)
            // increment value of residual level 
            context.commit('SET_RESIDUAL_SUBRISK_LABEL',{index, label })
          }
        })
      })
      // TODO: EXECUTE setResidualLevels and  saveProgress parallel
      // update residual level on each subrisk. This action is declare on risk module.
      paramsApi.subrisks = context.state.residualBySubrisk
      context.dispatch('risk/setResidualLevels', paramsApi, { root: true })

      // save progress in questions
      return context.dispatch('saveProgress', paramsApi)
    } catch (error) {
      throw error
    } 
  },
  //NOT USE 
  async initializeResultsOfQuestionnaire(state){
    try {
      state.commit('INITIALIZE_RESULTS_IN_DATABYTAB')
    } catch (error){
      throw error
    }
  },
  setEmpty(state, value){
    state.commit('SET_EMPTY_STATE', value)
  }
}

const mutations = {
  SET_SHOW_DRAWER(state, isOpened){
    state.showDrawer = isOpened
  },
  SET_QUESTION(state, question){
    state.questions.push(question)
  },
  SET_DATA_BY_TAB(state, data){
    state.dataByTab.push(data)
  },
  RESET_DATA_BY_TAB(state){
    state.dataByTab = []
  },
  INITIALIZE_RESULTS_IN_DATABYTAB(state){
    state.dataByTab.map( data => {
      data.results.map ( result => {
        result.value = -1
      })
    })
  },
  RESET_QUESTIONS(state){
    state.questions = []
  },
  ADD_ELEMENT_RESIDUAL_SUBRISK(state, element){
    state.residualBySubrisk.push(element)
  },
  INC_RESIDUAL_SUBRISK(state, element){
    state.residualBySubrisk[element.index].levelResidualAvg.value +=  element.total
  },
  SET_RESIDUAL_SUBRISK_LABEL(state, element){
    state.residualBySubrisk[element.index].levelResidualAvg.label =  element.label
  },
  RESET_RESIDUAL_SUBRISK(state){
    state.residualBySubrisk = []
  },
  SET_EMPTY_STATE(state, value){
    state.isEmpty = value
  },
  ADD_NEW_SUBRISK_RESULT(state, value){
    state.dataByTab.map(data =>{
      data.results.push(Object.assign({}, value))
    })
  },
  DELETE_NEW_SUBRISK_RESULT(state, id){
    state.dataByTab.map(data=>{
      data.results = data.results.filter(result => result.subrisk_id !== id)
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}