import apiTask from "src/api/task/task.js";
import apiGantt from "src/api/task/gantt";
import { Gantt, Data, Link } from "src/constructors/gantt.js";
import * as R from "ramda";
import store from "../";

function setHoursTasks(fecha) {
  fecha.setHours(23, 59);
  return fecha;
}

const state = {
  isEmpty: true,
  attendedPercentage: 0,
  ganttTask: { data: [], links: [] },
  loading: 0,
  columnAuxAudit: []
};
state.ganttTask.data;
const getters = {
  getGanttTasksLinks(state) {
    var data = state.ganttTask.data.map(task => new Data(task));
    var links = state.ganttTask.links.map(link => new Link(link));
    return { data, links };
  },
  getTextTask(state) {
    return getters.getGanttTasksLinks(state).data.map(task => {
      return R.pick(
        [
          "id",
          "text",
          "assigned",
          "linkedSubrisks",
          "type",
          "audit",
          "filesIdDocument",
          "controlFrecuency",
          "evaluationResult",
          "measureIndicator",
          "proceder",
          "responsibleArea"
        ],
        task
      );
    });
  },
  getTaskMonitoreoAssigned(state) {
    var result = getters
      .getGanttTasksLinks(state)
      .data.filter(task => task.type === "task");
    var filtro = [];
    //var fecha = new Date();
    result.map(item => {
      const Data = R.pick(
        [
          "id",
          "text",
          "assigned",
          "type",
          "linkedSubrisks",
          "controlFrecuency",
          "evaluationResult",
          "measureIndicator",
          "proceder",
          "responsibleArea",
          "next_day_monitory",
          "originType",
          "processOwner",
          "parent"
        ],
        item
      );
      if (
        Data.controlFrecuency === " " &&
        Data.proceder === " " &&
        Data.originType === "newTask"
      ) {
        Data.status = true;
        Data.measureIndicator = "";
        Data.controlFrecuency = null;
        // Agrega el valor de assigned al resposibleArea
        Data.responsibleArea = Data.assigned;
        filtro.push(Data);
      }
    });
    return filtro;
  },
  getTaskMonitoreo(state) {
    var result = getters
      .getGanttTasksLinks(state)
      .data.filter(task => task.type === "task");
    var filtro = [];
    result.map(item => {
      const Data = R.pick(
        [
          "id",
          "text",
          "assigned",
          "type",
          "linkedSubrisks",
          "controlFrecuency",
          "evaluationResult",
          "measureIndicator",
          "proceder",
          "responsibleArea",
          "next_day_monitory",
          "originType",
          "processOwner",
          "parent"
        ],
        item
      );
      let fechaMon = new Date(Data.next_day_monitory);
      if (Data.next_day_monitory !== "") {
        if (Data.evaluationResult === "Insatisfactorio") Data.resultado = 1;
        if (Data.evaluationResult === "Con Mejoras") Data.resultado = 2;
        if (Data.evaluationResult === "Satisfactorio") Data.resultado = 3;
        //   let fecha=fechaMon.getDate() + "-" + (fechaMon.getMonth() +1) + "-" + fechaMon.getFullYear();
        //  Data.next_day_monitory=fecha
        if (Data.resultado !== 0 && Data.proceder !== " ") Data.status = false;
        else Data.status = true;
        filtro.push(Data);
      } else {
        Data.status = false;
      }
    });
    return filtro;
  },
  getTaskAuditoria(state) {
    var result = getters
      .getGanttTasksLinks(state)
      .data.filter(task => task.type === "task");
    var filtro = [];
    result.map(item => {
      var Data = R.pick(
        [
          "id",
          "text",
          "cumple",
          "assigned",
          "observations",
          "responsableArea",
          "audit",
          "type",
          "status",
          "newFields",
          "filesIdDocument",
          "stepAudit",
          "processOwner",
          "satisfy"
        ],
        item
      );
      if (Data.satisfy === undefined || Data.satisfy === null) {
        Data.cumple = "";
        Data.observations = "";
        Data.responsableArea = "";
        Data.status = true;
      } else {
        Data.cumple = Data.satisfy;
        Data.observations = Data.observations;
        Data.responsableArea = Data.responsableArea;
        Data.status = false;
      }
      let files_origin = Data.filesIdDocument;
      let filtrado = new Array();
      filtrado = files_origin.filter(item => {
        if (item.origin === "auditoria") return item;
      });
      Data.files = filtrado;
      if (
        ((Data.stepAudit === 1 || Data.stepAudit === 3) &&
          store.state.user.myUser.isAuditor) ||
        (Data.stepAudit === 2 && Data.assigned === store.state.user.myUser._id)
      ) {
        filtro.push(Data);
      }
    });
    return filtro;
  },
  getCompletedTaskPercentage() {
    if (getters.getGanttTasksLinks(state).data.length < 1) {
      return { value: 100, see: false };
    }

    //var tasks = getters.getGanttTasksLinks(state).data.filter( task =>  (task.parent !== 0 && task.parent !== undefined) )
    var tasks = getters
      .getGanttTasksLinks(state)
      .data.filter(task => task.type === "task");
    var sum = 0;

    tasks.map(task => {
      sum += task.progress;
    });
    var valor = ((sum * 100) / tasks.length).toFixed(1);
    valor = parseFloat(valor);
    return { value: valor, see: true };
  },
  getNumTasksToDo() {
    if (getters.getGanttTasksLinks(state).data.length < 1) {
      return 0;
    }
    //var tasks = getters.getGanttTasksLinks(state).data.filter( task =>  (task.parent !== 0 && task.parent !== undefined) )
    var tasks = getters
      .getGanttTasksLinks(state)
      .data.filter(task => task.type === "task");

    var tasksToDo = tasks.filter(task => task.progress < 1).length;

    return tasksToDo;
  },
  getInfoTask(state) {
    if (getters.getGanttTasksLinks(state).data.length < 1) {
      return [];
    }

    //Excluir a padres
    //var tasks = getters.getGanttTasksLinks(state).data.filter( task =>  (task.parent !== 0 && task.parent !== undefined) )
    var tasks = getters
      .getGanttTasksLinks(state)
      .data.filter(task => task.type === "task");
    //Obtener Atrasados
    var atrasados = tasks.filter(
      task =>
        new Date() - setHoursTasks(new Date(task.end_date_orig)) > 0 &&
        task.progress < 1
    );
    // atrasados = atrasados.filter(task => task.progress < 1)

    //Obtener NO Atrasados
    var noAtrasados = tasks.filter(
      task => new Date() - setHoursTasks(new Date(task.end_date_orig)) <= 0
    );

    //Con progreso
    var conProgreso = noAtrasados.filter(
      task => task.progress > 0 && task.progress < 1
    );

    var numAtrasados, numProgreso, numSinEmpezar, numCompletados;

    numAtrasados = atrasados.length;
    numProgreso = conProgreso.length;
    numSinEmpezar = noAtrasados.filter(task => task.progress === 0).length;
    numCompletados = tasks.filter(task => task.progress === 1).length;

    var info = [
      {
        title: "Atrasados",
        total: numAtrasados,
        mode: "late",
        color: "#FF0040",
        description:
          "Total de tareas que no cumplen con la fecha de termino al día de hoy."
      },
      {
        title: "Sin empezar",
        total: numSinEmpezar,
        mode: "notStarted",
        color: "#2B85CD",
        description:
          "Toda aquella tarea que tiene porcentaje de avance igual a 0% y que no este catalogada como atrasada."
      },
      {
        title: "En progreso",
        total: numProgreso,
        mode: "inProgress",
        color: "#FABE3C",
        description:
          "Toda aquella tarea que tiene porcentaje mayor a 0% y que no este catalogada como atrasada."
      },
      {
        title: "Completados",
        total: numCompletados,
        mode: "completed",
        color: "#35A97A",
        description:
          "Tarea que ha sido terminada al 100% dentro de las fechas establecidas."
      }
    ];
    return info;
  },
  getInfoPlan(state) {
    if (getters.getGanttTasksLinks(state).data.length < 1) {
      return [];
    }
    //var planes = getters.getGanttTasksLinks(state).data.filter(plan => (plan.parent === 0 || plan.parent === undefined))
    var planes = getters
      .getGanttTasksLinks(state)
      .data.filter(plan => plan.type === "project");

    var info = planes.map(plan => {
      var estado;
      if (plan.progress === 1) {
        estado = "Completado";
      } else if (plan.progress === 0) {
        estado = "Sin empezar";
      } else {
        estado = "En progreso";
      }
      return {
        id: plan.id,
        plan: plan.text,
        assigned: plan.assigned,
        priority: plan.priority,
        state: estado
      };
    });
    return info;
  },
  getInfoChart(state) {
    if (getters.getGanttTasksLinks(state).data.length < 1) {
      return { data: [0, 0, 0, 0], see: false };
    }
    //var tasks = getters.getGanttTasksLinks(state).data.filter( task => ( task.parent !== 0 && task.parent !== undefined)  )
    var tasks = getters
      .getGanttTasksLinks(state)
      .data.filter(task => task.type === "task");
    var atrasados = tasks.filter(
      task =>
        new Date() - setHoursTasks(new Date(task.end_date_orig)) > 0 &&
        task.progress < 1
    );

    //Obtener No Atrasados
    var noAtrasados = tasks.filter(
      task => new Date() - setHoursTasks(new Date(task.end_date_orig)) <= 0
    );

    //Con progreso
    var conProgreso = noAtrasados.filter(
      task => task.progress > 0 && task.progress < 1
    );

    var numAtrasados, numProgreso, numSinEmpezar, numCompletados;
    var taskLength = tasks.length;
    numAtrasados = atrasados.length;
    numProgreso = conProgreso.length;
    numSinEmpezar = noAtrasados.filter(task => task.progress === 0).length;
    numCompletados = tasks.filter(task => task.progress === 1).length;

    var info = {
      data: [
        ((numAtrasados * 100) / taskLength).toFixed(1),
        ((numSinEmpezar * 100) / taskLength).toFixed(1),
        ((numProgreso * 100) / taskLength).toFixed(1),
        ((numCompletados * 100) / taskLength).toFixed(1)
      ],
      see: true
    };
    return info;
  }
};

const actions = {
  // metodo que trae toda la información de tareas
  async retriveTasks(context, paramsApi) {
    try {
      context.commit("RESET_TASKS");
      let response = await apiTask.getTasks(paramsApi);
      if (typeof response.data != "undefined") {
        let gantt = new Gantt(response.data);
        // feed gantt chart
        context.commit("SET_DATA_GANTT", gantt.data);
        context.commit("SET_LINKS_GANTT", gantt.links);
       // console.log("Response de retrieve tasks"+JSON.stringify(response.data))
        // create a new object for each question retrive from back-end
        if (context.state.ganttTask.data.length > 0) {
          context.commit("SET_EMPTY_STATE", false);
        } else {
          context.commit("SET_EMPTY_STATE", true);
        }
      }
    } catch (error) {
      throw error;
    }
  },

  // metodos en usados en el gantt
  async addTasks(context, paramsApi) {
    try {
      let response = await apiGantt.addData(paramsApi);
      await context.commit("ADD_TASK", paramsApi.data);
      // console.log("Add task", context.state.ganttTask.data)
      return response;
    } catch (error) {
      throw error;
    }
  },
  async updateTasks(context, paramsApi) {
    try {
      let response = await apiGantt.updateData(paramsApi);
      await context.commit("UPDATE_TASK", paramsApi.data);

      return response;
    } catch (error) {
      throw error;
    }
  },
  async deleteTasks(context, paramsApi) {
    try {
      let response = await apiGantt.deleteData(paramsApi);

      await context.dispatch("cascadeTaskDelete", paramsApi.id);

      await context.commit("DELETE_TASK", paramsApi.id);
      await context.commit("DELETE_TASK_LINKS", paramsApi.id);
      // console.log("Delete task", context.state.ganttTask.data)
      // console.log("Delete link", context.state.ganttTask.links)
      return response;
    } catch (error) {
      throw error;
    }
  },
  async cascadeTaskDelete(context, id) {
    let idSon = context.state.ganttTask.data.map(task => {
      return task.parent === id ? task.id : -1;
    });
    idSon = idSon.filter(id => id !== -1);
    for (var idS in idSon) {
      await context.dispatch("cascadeTaskDelete", idSon[idS]);
      await context.commit("DELETE_TASK", idSon[idS]);
      await context.commit("DELETE_TASK_LINKS", idSon[idS]);
    }
  },
  async deleteLinks(context, paramsApi) {
    try {
      let response = await apiGantt.deleteLink(paramsApi);
      await context.commit("DELETE_LINK", paramsApi.id);
      // console.log("Delete link", context.state.ganttTask.links)
      return response;
    } catch (error) {
      throw error;
    }
  },
  async addLinks(context, paramsApi) {
    try {
      let response = await apiGantt.addLink(paramsApi);
      await context.commit("ADD_LINK", paramsApi.ganttLinksParams);
      // console.log("Add Link", context.state.ganttTask.links)
      return response;
    } catch (error) {
      throw error;
    }
  },
  async massiveLoadTasks(context, paramsApi) {
    return new Promise((resolve, reject) => {
      apiTask
        .postTask(paramsApi)
        .then(res => {
          context
            .dispatch("retriveTasks", paramsApi)
            .then(() => {
              resolve(res);
            })
            .catch(error => {
              reject(error);
            });
        })
        .catch(error => {
          reject(error);
        });
    });
  },

  // manejo de archivos
  async updaloadFiles(context, paramsApi) {
    context.commit("ADD_LOADING", 2);
    try {
      let response = await apiTask.putFiles(paramsApi);
      if (response.status === 200) {
        context.commit("UPDATE_TASK_FILES", {
          response: response,
          id: paramsApi.id,
          origin: paramsApi.origin
        });
        return response;
      }
    } catch (error) {
      console.log(error);
      context.commit("ADD_LOADING", 0);
      throw error;
    }
  },
  async deleteFiles(context, paramsApi) {
    try {
      let response = await apiTask.deleteFiles(paramsApi);
      if (response.status === 200) {
        return response;
      }
    } catch (error) {
      console.log("ERR" + error);
      throw error;
    }
  },

  // auditoria

  async updateTaskAuditoria(context, paramsApi) {
    //crea item auditoria en task
    try {
      let response = await apiTask.putTasksAuditoria(paramsApi);
      // console.log('paramsApi :', paramsApi);
      if (response.status == 200) {
        let task = response.data.tasks.data.find(
          task => task.id == paramsApi.auditoria.id
        );
        context.commit("SET_TASK_AUDITORIA", task);
      }
      return response.status;
    } catch (error) {
      throw error;
    }
  },
  async getAttendedTaskTotalPercentage(context) {
    var paramsApi = {
      access_token: context.rootState.auth.token,
      subTypeOf: "section"
    };
    var info = await context.dispatch(
      "sections/getSectionsAndQuestions",
      paramsApi,
      { root: true }
    );
    var totalTasks = 0;
    info.map(sections => {
      totalTasks += sections.questions.length;
    });
    var numTasksNotAttended = context.state.ganttTask.data.length;
    context.commit(
      "SET_PERCENTAGE",
      Math.floor((numTasksNotAttended * 100) / totalTasks)
    );
  },
  async downloadFile(context, paramsApi) {
    try {
      let response = await apiTask.downloadFile(paramsApi);
      return response;
    } catch (error) {
      throw error;
    }
  },

  async downloadFiles(context, paramsApi) {
    try {
      let response = await apiTask.downloadFiles(paramsApi);
    } catch (error) {
      console.log(error);
      throw error;
    }
  },

  async addColumnAux(context, paramsApi) {
    try {
      let response = await apiTask.addColumnAux(paramsApi);
      //console.log("Response de addColumnAux:"+JSON.stringify(response))
      if (response.status == "200") {
        context.commit("ADD_COLUMN_TITLE", { title: paramsApi.fieldName });
        //console.log("Se agrego correctamente:"+paramsApi.fieldName)
        return response;
      } else console.log("Ocurre error" + response.status);
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  async removeColumnAux(context, paramsApi) {
    try {
      let indexFound = state.columnAuxAudit.findIndex(
        element => element.title === paramsApi.fieldName
      );
      let response = await apiTask.removeColumnAux(paramsApi);
      //   console.log("Response de removeColumnAux:"+JSON.stringify(response))
      if (response.status == "200") {
        context.commit("REMOVE_COLUMN_TITLE", indexFound);
        //    console.log("ELIMINA correctamente"+JSON.stringify(response))
        return response;
      }
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  async getColumnAuxAudit(context, data) {
    context.commit("CLEAN_COLUMN_TITLE")
    let titulo=[]
    let texto, tt
    data.forEach(element => {
      texto=JSON.stringify(element)
      tt=texto.split(':')
      tt=tt[0].substring(2, tt[0].length - 1)
      titulo.push(tt)
      context.commit("ADD_COLUMN_TITLE", { title: tt })
    })
  },
  removeAllColumn(context){
    context.commit("CLEAN_COLUMN_TITLE")
  }
  //------------------------------------------
};

const mutations = {
  RESET_TASKS(state) {
    state.ganttTask.data = [];
    state.ganttTask.links = [];
  },
  SET_TASK(state, task) {
    state.ganttTask.data.push(task);
    localStorage.setItem("localTasks", JSON.stringify(state.ganttTask.data));
  },
  SET_EMPTY_STATE(state, value) {
    state.isEmpty = value;
  },
  SET_PERCENTAGE(state, value) {
    state.attendedPercentage = value;
  },
  SET_DATA_GANTT(state, value) {
    state.ganttTask.data = value;
  },
  SET_LINKS_GANTT(state, value) {
    state.ganttTask.links = value;
  },
  UPDATE_TASK_MONITOR(state, value) {
    state.ganttTask.data.push(value);
    localStorage.setItem("localTasks", JSON.stringify(state.ganttTask.data));
  },
  ADD_TASK(state, data) {
    state.ganttTask.data.push(new Data(data));
  },
  UPDATE_TASK(state, data) {
    const newValues = state.ganttTask.data.map(task => {
      if (task.id === data.id) {
        var newTask = Object.assign(task, data);
        newTask.start_date = R.is(Date, newTask.start_date)
          ? newTask.start_date.toString()
          : newTask.start_date;
        newTask.end_date = R.is(Date, newTask.end_date)
          ? newTask.end_date.toString()
          : newTask.end_date;
        newTask.start_date_orig = undefined;
        newTask.end_date_orig = undefined;
        return new Data(newTask);
      } else {
        return task;
      }
    });
    state.ganttTask.data = newValues;
  },
  DELETE_TASK(state, id) {
    const newValues = state.ganttTask.data.filter(task => task.id !== id);
    state.ganttTask.data = newValues;
  },
  ADD_LINK(state, link) {
    state.ganttTask.links.push(new Link(link));
  },
  DELETE_LINK(state, id) {
    const newValues = state.ganttTask.links.filter(link => link.id !== id);
    state.ganttTask.links = newValues;
  },
  DELETE_TASK_LINKS(state, id) {
    const newValues = state.ganttTask.links.filter(
      link => link.source !== id && link.target !== id
    );
    state.ganttTask.links = newValues;
  },
  DELETE_SONS(state, id) {
    const newValues = state.ganttTask.data.filter(task => task.parent !== id);
    state.ganttTask.data = newValues;
  },
  SET_TASK_AUDITORIA(state, task) {
    let indexFound = state.ganttTask.data.findIndex(
      element => element.id == task.id
    );
    let foundTask = state.ganttTask.data[indexFound];
    foundTask.audit = {
      observations: task.observations,
      satisfy: task.satisfy
    };
    foundTask.observations = task.observations
      ? task.observations
      : foundTask.observations;
    foundTask.satisfy = task.satisfy ? task.satisfy : foundTask.satisfy;
    foundTask.processOwner = task.processOwner
      ? task.processOwner
      : foundTask.processOwner;
    foundTask.assigned = task.assigned ? task.assigned : foundTask.assigned;
    foundTask.stepAudit = task.stepAudit ? task.stepAudit : foundTask.stepAudit;
  },
  UPDATE_TASK_FILES(state, paramsApi) {
    var lista = paramsApi.response.data;
    const lista_filtrada = new Array();
    let DataBD = {
      fileName: lista.fileName,
      fileUrl: lista.fileUrl,
      size: lista.size,
      origin: lista.origin,
      key: lista.key
    };
    lista_filtrada.push(DataBD);
    let id = paramsApi.id;
    state.ganttTask.data.map(task => {
      if (task.id === id) {
        task.filesIdDocument = task.filesIdDocument.concat(lista_filtrada);
      }
    });
    localStorage.setItem("localTasks", JSON.stringify(state.ganttTask.data));
  },
  DELETE_FILE(state, file) {
    state.ganttTask.data.map(task => {
      if (task.id == file.id) {
        task.files = task.files.filter(fileU => fileU.key != file.keyFile);
      }
    });
  },
  UPDATE_FILES_TASK(state, data) {
    let fileKey = data.paramsApi.keyFile;
    let idTask = data.paramsApi.id;
    let archivos = null;
    let filtred = null;
    state.ganttTask.data.filter(task => {
      if (task.id === idTask) {
        archivos = task.filesIdDocument;
        filtred = archivos.map(id => {
          if (id.key !== fileKey) {
            return id;
          } else {
            console.log("Eliminar " + id.key);
          }
        });
      }
    });
  },
  ADD_LOADING(state, pct) {
    state.loading = pct;
  },
  FILES_FILTRED(state, params) {
    let origen = params.origin;
    let task = params.item;
    let filtrado = new Array();
    filtrado = task.filesIdDocument.filter(item => {
      if (item.origin === origen) return item;
    });
    task.filesIdDocument = filtrado;
  },
  ADD_COLUMN_TITLE(state, title) {
    //  console.log("Titulo ADD_COLUM_TITLE"+JSON.stringify(title))
    state.columnAuxAudit.push(title);
  },
  REMOVE_COLUMN_TITLE(state, indexFound) {
    state.columnAuxAudit.splice(indexFound, 1);
  },
  CLEAN_COLUMN_TITLE(state){
    state.columnAuxAudit=[]
  }
};
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
