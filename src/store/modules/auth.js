import auth from 'src/api/auth'

const state = {
  token: localStorage.getItem('access_token') || null,
  organizationId: localStorage.getItem('organizationId') || null,
  email: localStorage.getItem('email') || null,
}

const getters= {
  loggedIn(state) {
    return state.token !== null
  },
  getOrganizationId(state) {
    return state.organizationId !== null
  },
  getemail(state) {
    return state.email
  }
}

const actions = {
  
  signIn(context, credentials) {
    return auth.getToken(credentials)
    .then( response => {
      const access_token = response.data.token
      const organizationId= response.data.user.organization_id
      const email=response.data.user.email
      //localStorage.setItem('access_token', access_token)
      //context.commit('signIn', access_token)
      $cookies.set('token',access_token)
      localStorage.setItem('organizationId', organizationId)
      context.commit('setOrganizationId', organizationId)
      localStorage.setItem('email', email)
      context.commit('setemail', email)
    })
  },
  logout(context) {
    $cookies.remove('token')
    context.commit('LOGOUT')
  }
}
const mutations = {
  /*signIn(state, token) {
    state.token = token
  },*/
  LOGOUT(state) {
    state.token = null
    state.organizationId = null
    state.email = null
    localStorage.clear()
  },
  setOrganizationId(state, organizationId) {
    state.organizationId = organizationId
  },
  setemail(state, email) {
    state.email = email
  },

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}