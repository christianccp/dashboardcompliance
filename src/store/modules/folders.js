import folders from 'src/api/folders/folders.js'
//import {file} from 'src/constructors/file.js'
import Vue from 'vue'

const state = {
    /* folders: [
        {_id: 1, name: 'Declaración de misión, visión y valores'},
        {_id: 2, name: 'Código de conducta o ética'},
        {_id: 3, name: 'Manual organizacional'},
        {_id: 4, name: 'Politicas corporativas'},
    ], */
    folders: [],
    files: []
}

const getters = {

}

const actions = {
    async retrieveFolders (context, access_token) {
        try {
            context.commit('RESET_FOLDERS')
            let response = await folders.getFolders(access_token)
            context.commit('SET_FOLDERS',response.data)
          } catch (error) {
            throw error
          }
    },

    async addFolder (context, paramsApi) {
        try{
            let folder = await folders.insertFolder(paramsApi)
            let newFolder = folder.data
            let addFolder = { id: newFolder.id, name: newFolder.name}
            await context.commit("ADD_FOLDER", addFolder)
            return folder
        }catch(error){
            throw error
        }
    },

    async deleteFolder (context, paramsApi) {
        try{
            const { access_token, index } = paramsApi 
            let folder = context.state.folders[index]
            let response = await folders.deleteFolder( {access_token: access_token, id: folder.id })
            await context.commit("DELETE_FOLDER", folder.id)
            return response
        }catch(error){
            throw error
        }
    },

    async updateFolder (context, paramsApi) {
        try{
            const { access_token, index, name } = paramsApi 
            let folder = context.state.folders[index]
            let dataPut = { access_token: paramsApi.access_token, name: paramsApi.name, id: folder.id }   
            let response = await folders.updateFolder(dataPut)
            await context.commit("UPDATE_FOLDER", {index, name} )
            return response.status
        }catch(error){
            throw error
        }
    },
}

const mutations = {
    RESET_FOLDERS (state) {
        state.folders = []
    },
    SET_FOLDERS (state, folders) {
        state.folders = folders
    },
    ADD_FOLDER (state, folder) {
        state.folders.push(folder)
    },
    DELETE_FOLDER (state, id_folder) {
        /* const hello = state.folders.filter(folder => folders._id !== id_folder)
        state.folders = hello
        console.log(id_folder,state.folders); */
        let indexFound = state.folders.findIndex(element => element.id === id_folder)
        state.folders.splice(indexFound,1)
    },
    UPDATE_FOLDER (state, {index, name} ) {
        Vue.set(state.folders, index, Object.assign(state.folders[index],{name}))
    },
    RESET_FILES (state) {
        state.files = []
    },
    SET_FILES (state, files) {
        state.files = files.map((file)=>{
            return new File(file)
        })
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}