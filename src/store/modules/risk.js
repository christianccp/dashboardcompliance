import riskApi from "src/api/risk/risk.js";
import taskApi from "src/api/task/task.js";
import matrixApi from "src/api/risk/matrix.js";
import { Subrisk } from "src/constructors/subrisk.js";
import * as functions from "src/plugins/generalFunctions.js";
import * as ganttFunctions from "src/plugins/ganttFunctions.js";
import * as R from "ramda";
import Vue from "vue";

const getLocalSubrickTable = () => {
  try {
    return JSON.parse(localStorage.getItem("subriskTable")) !== null &&
      JSON.parse(localStorage.getItem("subriskTable")) !== undefined
      ? JSON.parse(localStorage.getItem("subriskTable"))
      : [];
  } catch (error) {
    console.error(error);
    return [];
  }
};

const state = {
  showPopupUmbral: false,
  subriskTable: getLocalSubrickTable(),
  totalSubrisks: 0
};

const getters = {
  isPopupUmbralVisible(state) {
    return state.showPopupUmbral;
  },
  // get title and id of subrisk
  getTitleAndIdSubrisks(state) {
    return state.subriskTable.map(subrisk => {
      return R.pick(["_id", "title"], subrisk);
    });
  },
  // indicate if questionnaire was answered
  isQuesFinished(state) {
    if (state.subriskTable.length < 1) {
      return false;
    }
    let flag = true;
    state.subriskTable.map(sub => {
      if (sub.levelResidualAvg.value < 0 && flag == true) {
        flag = false;
      }
    });
    return flag;
  },
  getDataGraph(state) {
    const dataGraph = {
      data: {
        labels: [],
        series: []
      }
    };
    state.subriskTable.map(sub => {
      dataGraph["data"].labels.push(sub.title);
      dataGraph["data"].series.push(sub.levelResidualAvg.value);
    });
    return dataGraph["data"];
  },

  getColorLevelGraf(state) {
    const BGColor = state.subriskTable.map(sub => {
      // if( sub.levelResidualAvg.value >= 0 && sub.levelResidualAvg.value <= 33){
      //   BGColor.push("rgba(247,27,20,0.9)")
      // }else if ( sub.levelResidualAvg.value > 33 && sub.levelResidualAvg.value <= 66){
      //   BGColor.push("rgba(255, 251, 0, 0.8)")
      // }else{
      //   BGColor.push("rgba(24,203,9,0.9)")
      // }
      return sub.levelResidualAvg.label;
    });

    return BGColor;
  },

  getLabelsGraph(state) {
    if (state.subriskTable == null) {
      return [];
    }
    const dataGraph = state.subriskTable.map(sub => {
      return { acron: functions.getAbbrev(sub.title), title: sub.title };
    });
    return dataGraph;
  },

  getMedioSubrisks(state) {
    return state.subriskTable != null
      ? state.subriskTable.filter(
          subrisk => subrisk.levelInhAvg.label === "Medio"
        ).length
      : 0;
  },

  getAltoSubrisks(state) {
    return state.subriskTable != null
      ? state.subriskTable.filter(
          subrisk => subrisk.levelInhAvg.label === "Alto"
        ).length
      : 0;
  },

  getValuesSubrisks(state, getters, contextStates, contextGetters) {
    var colors = [];
    if (state.subriskTable == null) {
      return { data: [], color: colors, see: false };
    }

    var levels = contextGetters["organization/levels"];

    const dataGraph = {
      data: {
        labels: [],
        series: []
      }
    };

    state.subriskTable.map(subrisk => {
      var color = "";
      levels.map(level => {
        if (subrisk.levelInhAvg.value >= level.value) {
          color = level.color;
        }
      });
      if (color !== "") {
        colors.push(color);
      } else {
        colors.push("darkgray");
      }
      dataGraph["data"].labels.push(subrisk.title);
      dataGraph["data"].series.push(subrisk.levelInhAvg.value);
    });

    var see = true;
    if (dataGraph["data"].series.length < 1) {
      see = false;
    }

    return { data: dataGraph["data"], color: colors, see: see };
  }
};

const actions = {
  async getSubriskParams(context, paramsApi) {
    try {
      let response = await matrixApi.getSubriskParams(
        paramsApi.access_token,
        paramsApi.subrisk_id
      );
      return response;
    } catch (error) {
      throw error;
    }
  },
  async selectSubrisks(context, paramsApi) {
    try {
      let subrisks = await riskApi.getSubrisks(paramsApi);
      context.commit("SET_SUBRISKS_TABLE", subrisks.data.rows);
      context.commit("SET_TOTAL_SUBRISKS", subrisks.data.count);
    } catch (error) {
      throw error;
    }
  },
  async setResidualLevels(context, paramsApi) {
    try {
      let response = await riskApi.setResidualLevels(paramsApi);
      if (response.status === 200) {
        // if residual level field was updated successfully, then update local vuex data too
        let { subrisks } = paramsApi;
        subrisks.map(subrisk => {
          context.commit("PARTIAL_UPDATE_SUBRISK", {
            _id: subrisk.subrisk_id,
            levelResidualAvg: subrisk.levelResidualAvg
          });
        });
      }
    } catch (error) {
      throw error;
    }
  },
  updateLabelInSubrisks(context) {
    try {
      let impacts = context.rootGetters["organization/impacts"];
      let probabilities = context.rootGetters["organization/probabilities"];
      let levels = context.rootGetters["organization/levels"];

      let catalogs = {
        impacts,
        probabilities,
        levels
      };
      context.commit("UPDATE_LABEL_SUBRISKS", catalogs);
    } catch (error) {
      console.error(error);
    }
  },
  async addSubrisk(context, paramsApi, context1, context2, context3) {
    try {
      let response = await riskApi.addSubrisk(paramsApi);
      context.commit("ADD_SUBRISK", response.data);
      let newSubriskObject = {
        subrisk_id: response.data._id,
        subriskTitle: response.data.title,
        value: -1
      };
      context.dispatch("question/addResultInDataByTab", newSubriskObject, {
        root: true
      });
      return response;
    } catch (error) {
      throw error;
    }
  },
  async addSubriskParams(context, paramsApi) {
    try {
      let response = await matrixApi.addSubriskParams(paramsApi);
      if (response.status === 200) {
        let { subriskId, levelInhAvg, probabilityAvg, impactAvg } = paramsApi;
        context.commit("PARTIAL_UPDATE_SUBRISK", {
          _id: subriskId,
          levelInhAvg,
          probabilityAvg,
          impactAvg
        });
      }
      return response;
    } catch (error) {
      throw error;
    }
  },
  async updateSubrisk(context, paramsApi) {
    try {
      let response = await riskApi.updateSubrisk(paramsApi);
      context.commit("UPDATE_SUBRISK", response.data);
      return response;
    } catch (error) {
      throw error;
    }
  },
  async updateSubriskParams(context, paramsApi) {
    try {
      let response = await matrixApi.updateSubriskParams(paramsApi);
      if (response.status === 200) {
        let { subriskId, levelInhAvg, probabilityAvg, impactAvg } = paramsApi;
        context.commit("PARTIAL_UPDATE_SUBRISK", {
          _id: subriskId,
          levelInhAvg,
          probabilityAvg,
          impactAvg
        });
        context.commit("UPDATE_SUBRISK", response.data);
      }
      return response.status;
    } catch (error) {
      throw error;
    }
  },
  async deleteSubrisk(context, paramsApi) {
    try {
      let response = await riskApi.deleteSubrisk(paramsApi);
      context.commit("DELETE_SUBRISK", paramsApi.id);
      context.dispatch("question/deleteResultInDataByTab", paramsApi.id, {
        root: true
      });
      return response.status;
    } catch (error) {
      throw error;
    }
  },
  async deleteSubriskParams(context, paramsApi) {
    try {
      let response = await matrixApi.deleteSubriskParams(paramsApi);
      if (response.status === 200) {
        let { subriskId, levelInhAvg, probabilityAvg, impactAvg } = paramsApi;
        context.commit("PARTIAL_UPDATE_SUBRISK", {
          _id: subriskId,
          levelInhAvg,
          probabilityAvg,
          impactAvg
        });
      }
      return response.status;
    } catch (error) {
      throw error;
    }
  },
  async sortDescSubriskTable(context) {
    context.commit("SORT_DESC_SUBRISK_TABLE");
  },
  async sortAscSubriskTable(context) {
    context.commit("SORT_ASC_SUBRISK_TABLE");
  },
  switchPopupUmbral(context) {
    context.commit("mutationUmbral", true);
  },
  async getTaskToDo(context, paramsApi) {
    var subrisks = context.state.subriskTable.map(subrisk => {
      return {
        subrisk_id: subrisk._id,
        proceder: {
          label: subrisk.proceder.label,
          value: subrisk.proceder.value
        }
      };
    });

    paramsApi.subrisks = subrisks;

    var response = await riskApi.setProceder(paramsApi);
    if (response.status !== 200) {
      return response;
    }

    return new Promise((resolve, reject) => {
      context
        .dispatch("sections/getSectionsAndQuestions", paramsApi, { root: true })
        .then(async info => {
          var { Inicio, Fin } = ganttFunctions.getValidDateMultipleTask();

          var idIncrement = 1;
          var tasksSections = info.map(dato => {
            //Filtrar preguntas que tengan como respuesta NO Y PARCIAL
            var questions = dato.questions.filter(ques => ques.result < 1);

            var tareaPadre = {
              id: idIncrement++,
              typeOf: dato.sections,
              text: dato.title,
              type: "project",
              start_date: Inicio,
              end_date: Fin,
              parent: 0,
              originType: "predefined"
            };
            var tasks = questions.map(ques => {
              //Obtener los Value de los subriesgos ligados a la pregunta
              var subrisk = ques.linkedSubrisks.map(title => {
                var sr = context.state.subriskTable.find(
                  subrisk => subrisk.title === title
                );
                if (sr !== undefined) {
                  return sr.proceder.value;
                } else {
                  return 1;
                }
              }); // END MAP ques.linkedSubrisks

              //Filtrar los value que sean diferentes a 1 (Aceptar)
              subrisk = subrisk.filter(value => value !== 1);

              //Si hay subriesgos que no son "Aceptar", darlo de alta como tarea
              if (subrisk.length > 0) {
                var task = {
                  id: idIncrement++,
                  text: ques.taskDescription
                    ? ques.taskDescription
                    : ques.title,
                  linkedSubrisks: ques.linkedSubrisks,
                  type: "task",
                  typeOf: dato.sections,
                  start_date: Inicio,
                  end_date: Fin,
                  next_day_monitory: Fin,
                  parent: tareaPadre.id,
                  originType: "predefined"
                };
                return task;
              }
            }); //END questions map
            tasks = tasks.filter(task => task !== undefined);
            if (tasks.length > 0) {
              tasks.push(tareaPadre);
            }
            return tasks;
          }); //END info map

          tasksSections = tasksSections.filter(x => x.length > 0);
          var totalTasks = [];
          tasksSections.map(task => {
            totalTasks = totalTasks.concat(task);
          });

          var param = {
            access_token: paramsApi.access_token,
            moduleId: paramsApi.moduleId,
            tasks: {
              data: totalTasks,
              links: [{ type: 0 }]
            }
          };
          return taskApi.postTask(param);
        })
        .then(res => {
          resolve(res);
        })
        .catch(error => {
          reject(error);
        });
    });
  }
};

const mutations = {
  mutationUmbral(state, flag) {
    state.showPopupUmbral = flag;
  },
  SET_SUBRISKS_TABLE(state, subrisks) {
    state.subriskTable = subrisks.map(subrisk => {
      return new Subrisk(subrisk);
    });
    localStorage.setItem("subriskTable", JSON.stringify(state.subriskTable));
  },
  UPDATE_LABEL_SUBRISKS(state, catalogs) {
    let { impacts, probabilities, levels } = catalogs;
    state.subriskTable.map(subrisk => {
      subrisk.impactAvg.label = functions.getLabel(
        subrisk.impactAvg.value,
        impacts
      );
      subrisk.probabilityAvg.label = functions.getLabel(
        subrisk.probabilityAvg.value,
        probabilities
      );
      subrisk.levelInhAvg.label = functions.getLabelLevel(
        subrisk.levelInhAvg.value,
        levels
      );
    });
    localStorage.setItem("subriskTable", JSON.stringify(state.subriskTable));
  },
  ADD_SUBRISK(state, data) {
    state.subriskTable.push(new Subrisk(data));
    localStorage.setItem("subriskTable", JSON.stringify(state.subriskTable));
  },
  UPDATE_SUBRISK(state, data) {
    let indexFound = state.subriskTable.findIndex(
      element => element._id == data._id
    );
    Vue.set(state.subriskTable, indexFound, new Subrisk(data));
    localStorage.setItem("subriskTable", JSON.stringify(state.subriskTable));
  },
  // update only the fields includes on input data. data object MUST have a field _id
  PARTIAL_UPDATE_SUBRISK(state, data) {
    let indexFound = state.subriskTable.findIndex(
      element => element._id == data._id
    );
    Object.assign(state.subriskTable[indexFound], data);
    localStorage.setItem("subriskTable", JSON.stringify(state.subriskTable));
  },
  DELETE_SUBRISK(state, idToDelete) {
    let indexFound = state.subriskTable.findIndex(
      element => element._id == idToDelete
    );
    state.subriskTable.splice(indexFound, 1);
    localStorage.setItem("subriskTable", JSON.stringify(state.subriskTable));
  },
  SET_TOTAL_SUBRISKS(state, count) {
    state.totalSubrisks = count;
  },
  SET_PROCEDER(state, param) {
    state.subriskTable[param.index].proceder = param.proceder;
    localStorage.setItem("subriskTable", JSON.stringify(state.subriskTable));
  },
  SORT_DESC_SUBRISK_TABLE(state) {
    state.subriskTable.sort(function(a, b) {
      var titlea = a.levelInhAvg.value;
      var titleb = b.levelInhAvg.value;
      return titleb - titlea;
    });
  },
  SORT_ASC_SUBRISK_TABLE(state) {
    state.subriskTable.sort(function(a, b) {
      var titlea = a.levelInhAvg.value;
      var titleb = b.levelInhAvg.value;
      return titlea - titleb;
    });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
