import files from 'src/api/folders/files.js'
import {File} from 'src/constructors/file.js'
import Vue from 'vue'

const state = {
    files: []
}

const getters = {

}

const actions = {
    async retrieveFiles (context, paramsApi) {
        try {
            context.commit('RESET_FILES')
            let response = await files.getFiles(paramsApi)
            context.commit('SET_FILES',response.data)
          } catch (error) {
            throw error
          }
    },
    async addFiles (context, paramsApi) {
        try{
            let response = await files.insertFiles(paramsApi)
            let newFile = response.data
            await context.commit("ADD_FILES", newFile)
        }catch(error){
            throw error
        }
    },
    async updateFiles (context, paramsApi) {
        try{
            let response = await files.updateFiles(paramsApi)
            let newFile = response.data
            await context.commit("UPDATE_FILES", newFile)
            return response
        }catch(error){
            throw error
        }
    },
    async setLogFiles (context, paramsApi) {
        try{
            let response = await files.setLogFiles(paramsApi)
            let newFile = response.data
            await context.commit("UPDATE_FILES", newFile)
            return response
        }catch(error){
            throw error
        }
    },
    async deleteFile (context, paramsApi) {
        try{
            let response = await files.deleteFile(paramsApi)
            await context.commit("DELETE_FILE", paramsApi.id)
            return response
        }catch(error){
            throw error
        }
    },
    async addVersionFile (context, paramsApi) {
        try{
            let response = await files.putVersionFile(paramsApi)
            await context.commit("UPDATE_FILES", response.data)
            return response
        }catch(error){
            throw error
        }
    },
    async deleteVersionFile (context, paramsApi) {
        try{
            let response = await files.deleteVersionFile(paramsApi)
            await context.commit("UPDATE_FILES", response.data)
            return response
        }catch(error){
            throw error
        }
    }
}

const mutations = {
    ADD_FILES (state, files) {
        state.files.push(new File (files))
    },
    DELETE_FILE (state, id_file) {
        let indexFound = state.files.findIndex(element => element.id === id_file)
        state.files.splice(indexFound,1)
    },
    UPDATE_FILES (state, fileInfo) {
        let index = state.files.findIndex(file => file.id == fileInfo.id)
        Vue.set(state.files, index, new File (fileInfo))
    },
    RESET_FILES (state) {
        state.files = []
    },
    SET_FILES (state, files) {
        state.files = files.map((file)=>{
            return new File(file)
        })
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}